const initialstate = {
  CustomerShipments: [],
  offers: [],
};

const customerShipmentReducer = (state = initialstate, action) => {
  if (action.type === "ADD_NEW_SHIPMENT_CUSTOMER") {
    console.log(action.data);
    return {
      ...state,
      CustomerShipments: [
        action.data.data.newshipment,
        ...state.CustomerShipments,
      ],
    };
  }
  if (action.type === "GET_SHIPMENT_CUSTOMER") {
    return {
      ...state,
      CustomerShipments: [...action.data.data.shipments],
    };
  }

  if (action.type === "ADD_OFFER")
    return {
      ...state,
      offers: [...action.data.data.offers, ...state.offers],
    };

  if (action.type === "ACCEPT_CUSTOMER_OFFER") {
    const updatedshipment = action.data.data.Shipment;
    const allcustomerShipments = [...state.CustomerShipments];
    const index = allcustomerShipments.findIndex(
      (shipment) => shipment._id === updatedshipment._id
    );
    allcustomerShipments[index] = updatedshipment;
    return {
      ...state,
      CustomerShipments: [...allcustomerShipments],
    };
  }

  return state;
};

export default customerShipmentReducer;

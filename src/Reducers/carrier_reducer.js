const initialstate = {
  auth_Token: "",
  Carrier: {},
  Carrier_Shipments: [],
  //any other customer object can be added here and later used
};

const carrierReducer = (state = initialstate, action) => {
  if (action.type === "LOGIN_CARRIER") {
    return {
      ...state,
      auth_Token: action.data.authToken,
      Carrier: { ...action.data.carrier },
    };
  }

  if (action.type === "GET_CARRIER_SHIPMENTS") {
    return {
      ...state,
      Carrier_Shipments: action.data.data.newshipments,
    };
  }

  if (action.type === "ADD_DRIVER_TRUCK") {
    const updatedshipment = action.data.data.Shipment;
    const allcarrierShipments = [...state.Carrier_Shipments];
    const index = allcarrierShipments.findIndex(
      (shipment) => shipment._id === updatedshipment._id
    );
    allcarrierShipments[index] = updatedshipment;
    return {
      ...state,
      Carrier_Shipments: [...allcarrierShipments],
    };
  }
  return state;
};

export default carrierReducer;

const initialstate = {
  customer_notification: [],
  carrier_notification: [],
};

const NotificationReducer = (state = initialstate, action) => {
  if (action.type === "CUSTOMER_NOTIFICATION") {
    return {
      ...state,
      customer_notification: [...action.data.Notifications],
    };
  }
  if (action.type === "CARRIER_NOTIFICATION") {
    return {
      ...state,
      carrier_notification: [...action.data.Notifications],
    };
  }
  return state;
};

export default NotificationReducer;

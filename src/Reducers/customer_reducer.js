const initialstate = {
  auth_Token: "",
  Customer: {},
  //any other customer object can be added here and later used
};

const customerReducer = (state = initialstate, action) => {
  if (action.type === "LOGIN_CUSTOMER") {
    return {
      ...state,
      auth_Token: action.data.authToken,
      Customer: { ...action.data.customer },
    };
  }

  if (action.type === "SIGNUP_CUSTOMER") {
    return {
      ...state,
      auth_Token: action.data.authToken,
      Customer: { ...action.data.data.newcustomer },
    };
  }
  return state;
};

export default customerReducer;

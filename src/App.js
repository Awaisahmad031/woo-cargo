import React, { Component } from "react";
import "./Styles/style.css";
import "./Styles/bootstrap.css";
import "./Styles/bootstrap-grid.min.css";
import { BrowserRouter, Route, Switch } from "react-router-dom";
import LogInForm from "../src/pages/Login";
import SignUp from "../src/pages/Signup";
import CustomerAccount from "../src/pages/Customer/CustomerAccount";
import ShippingDetailPage from "../src/pages/Customer/ShippingDetailPage";
import ShippingDetailPageCarrier from "./pages/Carrier/ShippingDetailPageCarrier";
import NewShipment from "../src/pages/Customer/NewShipment";
import Shipments from "../src/pages/Customer/Shipments";
import ChatWithCarrier from "./pages/Customer/ChatWithCarrier";
import ChatWithAdmin from "./pages/Customer/ChatWithAdmin";
import SearchShipment from "./pages/Carrier/SearchShipment";
import VehicalDashboard from "./pages/Carrier/VehicalDashboard";
import DriverDashboard from "./pages/Carrier/DriverDashboard";
import MyShipment from "./pages/Carrier/MyShipment";
import ProtectedRouteC from "./Components/Common/ProtectedRouteC";
import ProtectedRCarrier from "./Components/Common/ProtectedRCarrier";
// import ChatWindow from "./pages/Customer/ChatWithCarrier";
// import io from "socket.io-client";

export default class App extends Component {
  componentDidMount() {
    console.log("Page reload event registered");
    window.addEventListener("beforeunload", this.onUnload);
    // const notificationsocket = io(
    //   "http://127.0.0.1:8000/notifications?userid=5e919fd07f7b84027c2ebdca"
    // );
    // notificationsocket.on("connect", () => {
    //   console.log("connected to Notifications socket");
    // });
  }
  onUnload = () => {
    console.log("Page refreshed");
  };
  render() {
    return (
      <div style={{ background: "#f4f6fc", height: "100%" }}>
        <BrowserRouter>
          <Switch>
            {/* Form */}
            <Route
              path="/"
              exact
              render={(props) => <LogInForm {...props} />}
            />
            <Route path="/SignUp" render={(props) => <SignUp {...props} />} />
            {/* Customer Side */}
            <ProtectedRouteC path="/Shipments" component={Shipments} />
            <ProtectedRouteC
              path="/DetailPageCustomer/:id"
              component={ShippingDetailPage}
            />
            <ProtectedRouteC path="/NewShipment" component={NewShipment} />
            <ProtectedRouteC
              path="/CustomerAccount"
              component={CustomerAccount}
            />
            <ProtectedRouteC
              path="/ChatWithCarrier"
              component={ChatWithCarrier}
            />
            <ProtectedRouteC path="/ChatWithAdmin" component={ChatWithAdmin} />
            {/* Carrier Side */}
            <ProtectedRCarrier path="/CarrierHome" component={SearchShipment} />
            <ProtectedRCarrier path="/MyShipment" component={MyShipment} />
            <ProtectedRCarrier
              path="/DetailPageCarrier/:id"
              component={ShippingDetailPageCarrier}
            />
            <ProtectedRCarrier
              path="/vehicalDashboard"
              component={VehicalDashboard}
            />
            <ProtectedRCarrier
              path="/driverDashboard"
              component={DriverDashboard}
            />
          </Switch>
        </BrowserRouter>
      </div>
    );
  }
}

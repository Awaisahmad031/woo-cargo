import React, { Component } from "react";
import { Link } from "react-router-dom";
import Images from "../../Pics/Images";
import Progressbar from "./Progressbar";
import OfferModal from "../CarrierSide/OfferModal";
import "../../Styles/Card.css";

class Card extends Component {
  constructor(props) {
    super(props);
    this.state = {
      offerModalVisible: false,
      offerbutton: false,
      detailpagelink: "",
      openbuttonclickCustomer: false,
      progressvalue: 0,
    };
  }
  //---------When to show offer button-----------
  offerButton = () => {
    if (
      this.props.detailPage === "CarrierHome" &&
      this.props.list.offer_accepted === undefined
    ) {
      this.setState({ offerbutton: true });
    } else {
      this.setState({ offerbutton: false });
    }
  };
  //----------when click on offer button----
  modalVisibility = () => {
    this.setState({ offerModalVisible: !this.state.offerModalVisible });
  };
  componentDidMount() {
    this.offerButton();
    this.handleDetailPageLink();
    this.handleProgressValue();
  }
  // detail page check either carrier side or customer side
  handleDetailPageLink = () => {
    if (this.props.detailPage === "Shipments")
      return this.setState({
        detailpagelink: "/DetailPageCustomer",
        openbuttonclickCustomer: true,
      });
    else
      return this.setState({
        detailpagelink: "/DetailPageCarrier",
        openbuttonclickCustomer: false,
      });
  };
  // for checking progress value
  handleProgressValue = () => {
    switch (this.props.list.shipment_status) {
      case "WAITING": {
        this.setState({ progressvalue: 20 });
        break;
      }
      default:
        return null;
    }
  };
  render() {
    const {
      _id,
      shipment_status,
      shipment_type,
      pickup_location,
      dropoff_location,
      pickup_date = "2020-04-03",
      dropoff_date = "2020-02-02",
    } = this.props.list;
    const pickupdate = pickup_date.slice(0, 10);
    const dropoffdate = dropoff_date.slice(0, 10);
    return (
      <div className="container bgcolor mb-3 shadow-sm mt-2">
        <div className="row py-3">
          <div className="col-3">
            <div className="ID">
              <p className="numbercolor">{_id}</p>
              <p className="blue_text">{shipment_type}</p>
              <p className="black_text">{shipment_status}</p>
              {shipment_status === "WAITING" ? (
                <Progressbar barvalue={this.state.progressvalue} />
              ) : null}
            </div>
          </div>
          <div className="col-4">
            <div className="row">
              <div className="col-3 mr-0 pr-0">
                <img
                  src={Images.pickupicon}
                  className="img-fluid d-inline"
                  alt="arrowup"
                  style={{ width: "30px", height: "30px" }}
                />
              </div>
              <div className="col-9 ml-0 pl-0">
                <span className="blue_text text-center addresssize ">
                  {pickup_location.address}
                </span>
                <br />
                <br />
                <p className="startingdate black_text">{pickupdate}</p>
                <p className="time numbercolor light_gray_text">{/* time */}</p>
              </div>
            </div>
          </div>
          <div className="col-4">
            <div className="row">
              <div className="col-3 mr-0 pr-0">
                <img
                  src={Images.deliveryicon}
                  className="img-fluid d-inline"
                  alt="arrowdown"
                  style={{ width: "30px", height: "30px" }}
                />
              </div>
              <div className="col-9 ml-0 pl-0">
                <span className="blue_text  ">
                  {dropoff_location.address}
                </span>
                <br />
                <br />
                <p className="startingdate black_text">{dropoffdate}</p>

                <p className="time numbercolor light_gray_text">{/* time */}</p>
              </div>
            </div>
          </div>
          <div className="col-1">
            <p className="blueandbold mt-5">
              {this.props.list.price ? this.props.list.price : "---"} $
            </p>
            {/*--------  --------Show offer button---------------- */}
            {this.state.offerbutton ? (
              <React.Fragment>
                <button className="btn btnsize" onClick={this.modalVisibility}>
                  Offer
                </button>
                {this.state.offerModalVisible && (
                  <OfferModal
                    shipmentobj={this.props.list}
                    offerModalVisible={this.state.offerModalVisible}
                    modalVisibility={this.modalVisibility}
                  />
                )}
                {/*--------  --------Show offer End---------------- */}
              </React.Fragment>
            ) : (
              <Link to={this.state.detailpagelink + `/${_id}`}>
                {/*-------------show open button---------------------- */}
                {this.state.openbuttonclickCustomer ? (
                  <button className="btn btnsize">Open</button>
                ) : (
                  <button className="btn btnsize">Open</button>
                )}
              </Link>
            )}
          </div>
        </div>
      </div>
    );
  }
}

export default Card;

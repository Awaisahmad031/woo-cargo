import React from "react";
import "../../Styles/SwitchSlider.css";

export default function CheckboxSlider({ labelTitle, Notification }) {
  return (
    <React.Fragment>
      <label className="text-muted">{labelTitle}</label>
      <br />
      <label className="switch" type="btn" onClick={Notification}>
        <input type="checkbox" />
        <span className="slider round"></span>
      </label>
    </React.Fragment>
  );
}

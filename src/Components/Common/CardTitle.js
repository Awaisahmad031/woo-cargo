import React, { Component } from "react";
import "../../Styles/CustomerHome.css";

export default class CardTitle extends Component {
  render() {
    return (
      <div className="container mt-5">
        <div className="row">
          <div className="col-3">
            <span className="numbercolor">DESCRIPTION ID</span>
          </div>
          <div className="col-4">
            <span className="numbercolor">PICKUP LOCATION</span>
          </div>
          <div className="col-4">
            <span className="numbercolor">DELIVERY LOCATION</span>
          </div>
          <div className="col-1">
            <span className="numbercolor">PRICE</span>
          </div>
        </div>
      </div>
    );
  }
}

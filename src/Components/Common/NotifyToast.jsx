import React from "react";

export default function NotifyToast({ notify }) {
  return (
    <div
      className="container p-2"
      style={{
        background: "#ffffff",
        borderRadius: "5px",
        marginBottom: "10px",
        width: "350px",
        marginBottom:5
      }}
    >
      <p className="black_text">{notify.notification_type}</p>
      <p>{notify.notification_title}</p>
      <p className="gray_text">{notify.notification_content}</p>
    </div>
  );
}

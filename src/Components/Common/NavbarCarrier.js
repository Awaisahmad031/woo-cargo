import React, { Component } from "react";
import Images from "../../Pics/Images";
import NotificationCarrier from "./carrierNotification";
import { NavLink, withRouter, Link } from "react-router-dom";
import { removeToken } from "../../LocalStorage/Auth";
import "../../Styles/NavBar.css";
class NavBarCarrier extends Component {
  state = {
    showNotification: false,
  };
  handleNotification = () => {
    this.setState({ showNotification: !this.state.showNotification });
  };
  signOutCarrier = () => {
    removeToken();
  };
  render() {
    return (
      <nav className="navbar navbar-expand-lg navbar-light bg-white">
        <div className="container">
          <div>
            <span className="navbar-brand">
              <img src={Images.logo} className="img-fluid" alt="logo"></img>
              <p className="d-inline logotext__color">WooCargo</p>
            </span>
          </div>

          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <div className="border--color_Carrier justify-center">
              <ul className="navbar-nav">
                <li className="nav-item">
                  <NavLink
                    to="/CarrierHome"
                    style={{ fontWeight: 500 }}
                    activeClassName="active"
                    className="nav--item"
                  >
                    Search Shipments
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink
                    to="/MyShipment"
                    style={{ fontWeight: 500 }}
                    activeClassName="active"
                    className="nav--item"
                  >
                    My Shipments
                  </NavLink>
                </li>
                <li className="nav-item dropdown">
                  <Link
                    to="/"
                    className="nav--item dropdown-toggle"
                    style={{ fontWeight: 500 }}
                    id="navbarDropdown"
                    role="button"
                    data-toggle="dropdown"
                    aria-haspopup="true"
                    aria-expanded="false"
                  >
                    Chat
                  </Link>
                  <div
                    className="dropdown-menu"
                    aria-labelledby="navbarDropdown"
                  ></div>
                </li>
                <li className="nav-item">
                  <NavLink
                    to="/vehicalDashboard"
                    style={{ fontWeight: 500 }}
                    activeClassName="active"
                    className="nav--item"
                  >
                    Vehicles
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink
                    to="/driverDashboard"
                    style={{ fontWeight: 500 }}
                    activeClassName="active"
                    className="nav--item"
                  >
                    Drivers
                  </NavLink>
                </li>
              </ul>
            </div>
          </div>
          <div className="ml-auto">
            {/*-------NOTIFICATION------- */}
            <div className="dropdown d-inline-block">
              <img
                src={Images.bellicon}
                alt="bellicon"
                className="dropdown-toggle img-fluid mr-3"
                style={{ width: "24px", height: "24px", cursor: "pointer" }}
                id="navbarDropdown"
                role="button"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
                onClick={this.handleNotification}
              />
              <div
                className="dropdown-menu dropdown-menu-right"
                style={{
                  background: "#f4f6fc",
                  borderRadius: "10px",
                  cursor: "pointer",
                }}
                aria-labelledby="navbarDropdown"
              >
                {/* notification */}
                {this.state.showNotification && <NotificationCarrier />}
              </div>
            </div>

            {/* profile pic */}
            <div className="dropdown d-inline-block">
              <img
                src={Images.userpic}
                alt="userpic"
                className="dropdown-toggle img-fluid mr-3"
                id="navbarDropdown"
                role="button"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
                style={{
                  width: "40px",
                  height: "40px",
                  cursor: "pointer",
                }}
              />
              <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                <li
                  className="dropdown_item"
                  style={{ cursor: "pointer" }}
                  onClick={this.signOutCarrier}
                >
                  SignOut
                </li>
              </div>
            </div>
          </div>
          <button
            className="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon"></span>
          </button>
        </div>
      </nav>
    );
  }
}
export default withRouter(NavBarCarrier);

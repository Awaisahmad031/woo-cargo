import React, { Component } from "react";
import "../../Styles/CustomerHome.css";

export default class FilterModule extends Component {
  render() {
    const { button_select, buttonText, onPressButton } = this.props;
    return (
      <React.Fragment>
        <button
          onClick={() => {
            onPressButton(buttonText);
          }}
          className={
            button_select === buttonText ? "button button_active" : "button"
          }
        >
          {buttonText}
        </button>
      </React.Fragment>
    );
  }
}

import React from "react";
import NavBar from "./NavBar";
import { Link } from "react-router-dom";
import Images from "../../Pics/Images";

export default function Empty() {
  return (
    <React.Fragment>
      <NavBar />
      <div className="min_Height">
        <div className="container pt-5">
          <div className="row">
            <div className="col-md-10">
              <span className="vd-title">My Shipments</span>
              <span className="vd-total ml-3">0 Total</span>
              <div className="position_emptyBox">
                <img
                  src={Images.addbox}
                  alt="add_box"
                  className="d-block mb-2"
                />
                <Link to="/NewShipment">
                  <button className="btn btn-primary">Add a Shipment</button>
                </Link>
              </div>
            </div>
          </div>
        </div>
      </div>
    </React.Fragment>
  );
}

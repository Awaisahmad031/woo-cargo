import React from "react";
import { Route, Redirect } from "react-router-dom";
import { getCustomer } from "../../LocalStorage/Auth";

const ProtectedRouteC = ({ path, component: Component, render }) => {
  return (
    <Route
      path={path}
      render={(props) => {
        if (!getCustomer()) return <Redirect to="/" />;
        return Component ? <Component {...props} /> : render(props);
      }}
    />
  );
};

export default ProtectedRouteC;

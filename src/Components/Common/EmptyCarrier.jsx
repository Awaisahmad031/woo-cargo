import React, { Component } from "react";
import NavBarCarrier from "./NavbarCarrier";
import AddDriverModal from "../CarrierSide/AddDriverModal";
import AddVehicalModal from "../CarrierSide/AddVehicalModal";
import Modal from "react-bootstrap/Modal";
import "../../Styles/CustomerHome.css";

export default class EmptyCarrier extends Component {
  state = {
    modalDriver: false,
    modalVehical: false,
  };
  handleModalDriver = () => {
    this.setState({ modalDriver: !this.state.modalDriver });
  };
  handleModalVehical = () => {
    this.setState({ modalVehical: !this.state.modalVehical });
  };
  render() {
    const { title } = this.props;
    return (
      <React.Fragment>
        <NavBarCarrier />
        <div className="min_Height">
          <div className="container pt-5">
            <div className="row">
              <div className="col-md-10">
                <span className="vd-title">{this.props.title}</span>
                <span className="vd-total ml-3">0 Total</span>
              </div>
            </div>
          </div>
          {title === "Driver Dashboard" && (
            <button
              className="btn btn-primary positionCenter"
              onClick={this.handleModalDriver}
            >
              CLICK TO ADD DRIVER
            </button>
          )}
          {title === "Vehicle Dashboard" && (
            <button
              className="btn btn-primary positionCenter"
              onClick={this.handleModalVehical}
            >
              CLICK TO ADD VEHICAL
            </button>
          )}
          {/*--------DRIVER MODAL------ */}
          <Modal
            size="lg"
            show={this.state.modalDriver}
            onHide={this.handleModalDriver}
            backdrop="static"
            animation="true"
            aria-labelledby="example-modal-sizes-title-lg"
          >
            <AddDriverModal
              handleModal={this.handleModalDriver}
              title="AddDriver"
            />
          </Modal>
          {/*---------VEHICAL MODAL-------- */}
          <Modal
            size="lg"
            show={this.state.modalVehical}
            onHide={this.handleModalVehical}
            backdrop="static"
            animation="true"
            aria-labelledby="example-modal-sizes-title-lg"
          >
            <AddVehicalModal
              handleModal={this.handleModalVehical}
              title="AddVehical"
            />
          </Modal>
        </div>
      </React.Fragment>
    );
  }
}

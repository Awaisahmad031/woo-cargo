import React from "react";
import "../../Styles/Message.css";
import { getCustomerObj } from "../../LocalStorage/Auth";

export default function Message(props) {
  return (
    <React.Fragment>
      {/*--------------customer---------- */}
      {props.user === getCustomerObj().id ? (
        <div className="messageContainer justifyEnd">
          <div className="messageBox backgroundBlue shadow-sm">
            <p className="messageText colorWhite">{props.text}</p>
          </div>
        </div>
      ) : (
        <div className="messageContainer justifyStart">
          <div className="messageBox backgroundLight shadow-sm">
            <p className="messageText colorDark">{props.text}</p>
          </div>
          <p className="sentText">You</p>
        </div>
      )}
    </React.Fragment>
  );
}

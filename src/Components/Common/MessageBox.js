import React, { Component } from "react";
import Message from "./Message";
export default class MessageBox extends Component {
  componentDidMount() {
    this.scrollToBottom();
  }
  componentDidUpdate() {
    this.scrollToBottom();
  }
  scrollToBottom = () => {
    this.messagesEnd.scrollIntoView({ behavior: "smooth" });
  };
  render() {
    return (
      <React.Fragment>
        {this.props.message.map((msg, i) => {
          return (
            <Message key={i} user={msg.from_user} text={msg.msg_content} />
          );
        })}
        {/*---------FOR BOTTON SCROLLING---------- */}
        <div
          style={{ float: "left", clear: "both" }}
          ref={(el) => {
            this.messagesEnd = el;
          }}
        ></div>
      </React.Fragment>
    );
  }
}

import React, { Component } from "react";

class InputField extends Component {
  render() {
    return (
      <React.Fragment>
        <label
          className="font-weight-bolder"
          style={{ color: "#B0BAC9", fontSize: "15px" }}
        >
          {this.props.labeltitle}
        </label>
        <input
          type={this.props.typename}
          name={this.props.fieldname}
          className="form-control"
          value={this.props.value}
          onChange={this.props.changeHandler}
        />
        {this.props.error && (
          <div className="alert alert-danger" style={{ fontSize: "14px" }}>
            {this.props.error}
          </div>
        )}
      </React.Fragment>
    );
  }
}

export default InputField;

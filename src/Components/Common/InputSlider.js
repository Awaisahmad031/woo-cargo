import React, { useState } from "react";
import Slider from "react-input-slider";

export default function InputSlider() {
  const [state, setState] = useState({ x: 20 });

  return (
    <React.Fragment>
      <div className="float-right">{state.x + " km"}</div>
      <Slider
      styles={{
        
        track: {
          backgroundColor: 'rgba(46,91,255,0.2)',
          height:4
        },
        active: {
          backgroundColor: '#2E5BFF'
          
        },
        thumb: {
          width: 15,
          height: 15,
        },
        disabled: {
          opacity: 0.9
        }
      }}
        axis="x"
        step={1}
        min={0}
        max={100}
        x={state.x}
        onChange={({ x }) => setState({ x })}
      />
    </React.Fragment>
  );
}

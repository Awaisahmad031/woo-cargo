import React from "react";
const Option = ({ labelTitle, name, Options, Value, changeHandler }) => {
  return (
    <React.Fragment>
      <label className="text-muted">{labelTitle}</label>
      <select
        name={name}
        value={Value}
        onChange={changeHandler}
        className="form-control"
      >
        <option value="">Select</option>
        {Options.map(item => {
          return (
            <option key={item.id} value={item.value}>
              {item.value}
            </option>
          );
        })}
      </select>
    </React.Fragment>
  );
};
export default Option;

import React from "react";
import Images from "../../Pics/Images";

export default function LocationIcon() {
  return (
    <React.Fragment>
      <div className="col-1 mr-0 pr-0">
        <img
          src={Images.pickupicon}
          alt="icon"
          style={{ width: "30px", height: "30px" }}
          className="img-fluid"
        />
      </div>
      <div className="col-5 ml-0 pl-0">
        <hr style={{ background: "rgba(46,91,255,0.2)" }} />
      </div>
      <div className="col-6">
        <img
          src={Images.deliveryicon}
          alt="icon"
          style={{ width: "30px", height: "30px" }}
          className="img-fluid d-block"
        />
      </div>
    </React.Fragment>
  );
}

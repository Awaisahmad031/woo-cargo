import React, { Component } from "react";
import "../../Styles/LogInForm.css";
import Images from "../../Pics/Images";

export default class LoginBox extends Component {
  link = (data) => {
    this.props.link(data);
  };
  handle_click = () => {
    if (this.props.title === "Customer") {
      this.link("/Shipments");
    } else {
      this.link("/CarrierHome");
    }
  };
  render() {
    return (
      <div
        onClick={this.handle_click}
        className={
          this.props.box_clicked
            ? "box1 box_select text-center p-3 mr-1"
            : "box1 text-center p-3 mr-1"
        }
      >
        <img src={Images.signupicon1} alt="icon" className="box_icon"></img>
        <br />
        <p className="font-weight-bold">{this.props.title}</p>
        <p className="text--color">Full access to all setting</p>
      </div>
    );
  }
}

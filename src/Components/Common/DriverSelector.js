import React from "react";
const DriverSelector = ({ labelTitle, name, Option, value, changeHandler }) => {
  return (
    <React.Fragment>
      <label style={{ color: "#B0BAC9", fontSize: "16px" }}>{labelTitle}</label>
      <select
        name={name}
        id={name}
        value={value}
        onChange={changeHandler}
        className="form-control"
      >
        <option value="Select"></option>
        {Option.map((item, index) => {
          return (
            <option key={index} value={item.driver_name}>
              {item.driver_name}
            </option>
          );
        })}
      </select>
    </React.Fragment>
  );
};
export default DriverSelector;

import React, { Component } from "react";
import "../../Styles/LogInForm.css";
import Images from "../../Pics/Images";

export default class SignupBox extends Component {
  render() {
    return (
      <div className="box1 box_select text-center p-3">
        <img src={Images.signupicon1} alt="icon" className="box_icon"></img>
        <br />
        <p className="font-weight-bold">{this.props.title}</p>
        <p className="text--color">Full access to all setting</p>
      </div>
    );
  }
}

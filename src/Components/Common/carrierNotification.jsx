import React, { Component } from "react";
import { getCarrierNotification } from "../../Actions/actioncreators";
import { connect } from "react-redux";
import NotifyToast from "./NotifyToast";
import { getCarrier, getCarrierUserId } from "../../LocalStorage/Auth";
import "../../Styles/NavBar.css";

class customerNotification extends Component {
  state = {
    notification: [],
  };
  componentDidMount() {
    const token = getCarrier();
    const { id } = getCarrierUserId;
    const carrierid = {
      id: id,
    };
    const config = {
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
      },
    };
    if (this.props.notification.length === 0) {
      this.props.getNotification(carrierid, config, this.onSuccessCallback);
    }
    this.setState({ notification: this.props.notification });
  }
  onSuccessCallback = () => {
    console.log("successfully fetch notification", this.props.notification);
    this.setState({ notification: this.props.notification });
  };

  render() {
    if (this.state.notification.length === 0)
      return (
        <div
          className="container text-center"
          style={{
            background: "#ffffff",
            borderRadius: "20px",
            width: "150px",
          }}
        >
          No Notification
        </div>
      );
    return (
      <div
        style={{
          width: "350px",
          maxHeight: "60vh",
          overflowY: "scroll",
          overflowX: "none",
        }}
      >
        {this.state.notification.map((notify, index) => {
          return <NotifyToast key={index} notify={notify} />;
        })}
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    notification: state.NotificationReducer.carrier_notification,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    getNotification: (customerid, config, onSuccessCallback) =>
      dispatch(getCarrierNotification(customerid, config, onSuccessCallback)),
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(customerNotification);

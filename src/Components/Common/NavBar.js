import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import Images from "../../Pics/Images";
import { Link } from "react-router-dom";
import { removeToken } from "../../LocalStorage/Auth";
import Notification from "./customerNotification";
import "../../Styles/NavBar.css";
class NavBar extends Component {
  state = {
    showNotification: false,
  };
  handleNotification = () => {
    this.setState({ showNotification: !this.state.showNotification });
  };
  signOutCustomer = () => {
    removeToken();
  };
  render() {
    return (
      <nav className="navbar navbar-expand-lg navbar-light bg-white">
        <div className="container">
          <div>
            <span className="navbar-brand">
              <div className="d-flex align-item-center">
                <img src={Images.logo_with_text} alt="logo" />
              </div>
            </span>
          </div>

          <div className="collapse navbar-collapse" id="navbarSupportedContent">
            <div className="border--color">
              <ul className="navbar-nav">
                <li className="nav-item">
                  <NavLink
                    to="/Shipments"
                    style={{ fontWeight: 500 }}
                    className="px-4"
                    activeClassName="active"
                    className="nav--item"
                  >
                    Home
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink
                    to="/ChatWithCarrier"
                    style={{ fontWeight: 500 }}
                    activeClassName="active"
                    className="nav--item"
                  >
                    Chat with Carrier
                  </NavLink>
                </li>
                <li className="nav-item">
                  <NavLink
                    to="/ChatWithAdmin"
                    style={{ fontWeight: 500 }}
                    activeClassName="active"
                    className="nav--item"
                  >
                    Chat with Admin
                  </NavLink>
                </li>
              </ul>
            </div>
          </div>
          <div className="ml-auto">
            <div className="dropdown d-inline-block">
              <img
                src={Images.bellicon}
                className="dropdown-toggle img-fluid mr-3"
                style={{ width: "24px", height: "24px", cursor: "pointer" }}
                id="navbarDropdown"
                role="button"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
                onClick={this.handleNotification}
              />
              <div
                className="dropdown-menu dropdown-menu-right"
                style={{
                  background: "#f4f6fc",
                  borderRadius: "10px",
                  cursor: "pointer",
                }}
                aria-labelledby="navbarDropdown"
              >
                {/* notification */}
                {this.state.showNotification && <Notification />}
              </div>
            </div>

            {/* profile pic */}
            <div className="dropdown d-inline-block">
              <img
                src={Images.userpic}
                alt="userpic"
                className="dropdown-toggle img-fluid"
                id="navbarDropdown"
                role="button"
                data-toggle="dropdown"
                aria-haspopup="true"
                aria-expanded="false"
                style={{
                  width: "40px",
                  height: "40px",
                  cursor: "pointer",
                }}
              />
              <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                <Link to="/CustomerAccount">
                <li
                  className="dropdown_item"
                  style={{ cursor: "pointer" }}
                >
                 Settings 
                </li>
                </Link>
                <li
                  className="dropdown_item"
                  style={{ cursor: "pointer" }}
                  onClick={this.signOutCustomer}
                >
                  SignOut
                </li>
              </div>
            </div>
          </div>

          {/* button toggler */}
          <button
            className="navbar-toggler"
            type="button"
            data-toggle="collapse"
            data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon"></span>
          </button>
        </div>
      </nav>
    );
  }
}
export default NavBar;

import React from "react";

export default function RowRemoval({ data, type }) {
  const { packing, fromfloor, tofloor, assembly, disassembly } = data;
  return (
    <div className="row mt-4">
      <div className="col-2">
        <span className="offer-description text-uppercase d-block">Type</span>
        <span className="offer-notes">{type}</span>
      </div>
      <div className="col-2">
        <span className="offer-description text-uppercase d-block">
          Packing
        </span>
        <span className="offer-notes">{`${packing}`}</span>
      </div>
      <div className="col-2">
        <span className="offer-description text-uppercase d-block">Floor</span>
        <span className="offer-notes">{fromfloor}</span>
      </div>

      <div className="col-2">
        <span className="offer-description text-uppercase d-block">
          TO Floor
        </span>
        <span className="offer-notes">{tofloor}</span>
      </div>
      <div className="col-2">
        <span className="offer-description text-uppercase d-block">
          Assembly
        </span>
        <span className="offer-notes">{`${assembly}`}</span>
      </div>
      <div className="col-2">
        <span className="offer-description text-uppercase d-block">
          Disassembly
        </span>
        <span className="offer-notes">{`${disassembly}`}</span>
      </div>
    </div>
  );
}

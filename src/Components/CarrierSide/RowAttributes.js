import React from "react";

export default function RowAttributes({ data, type }) {
  const { packing, dimensions, quantity, weight, height } = data;
  return (
    <div className="row mt-4">
      <div className="col-2">
        <span className="offer-description d-block">Type</span>
        <span className="offer-notes">{type}</span>
      </div>
      <div className="col-2">
        <span className="offer-description d-block">Dimensions</span>
        <span className="offer-notes">{dimensions}</span>
      </div>
      <div className="col-2">
        <span className="offer-description d-block">
          Packing
        </span>
        <span className="offer-notes">{packing==true? 'Yes' : 'No'}</span>
      </div>
      <div className="col-2">
        <span className="offer-description d-block">
          Quantity
        </span>
        <span className="offer-notes">{quantity}</span>
      </div>
      <div className="col-2">
        <span className="offer-description d-block">Weight</span>
        <span className="offer-notes">{weight}</span>
      </div>
      <div className="col-2">
        <span className="offer-description d-block">Height</span>
        <span className="offer-notes">{height ? height : "----"}</span>
      </div>
    </div>
  );
}

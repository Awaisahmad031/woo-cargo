import React, { Component } from "react";
import OfferModalForm from "./OfferModalForm";
import LocationIcon from "../Common/LocationIcon";
import Modal from "react-bootstrap/Modal";
import RowAttributes from "./RowAttributes";
import RowRemoval from "./RowRemoval";
import { connect } from "react-redux";
import "../../Styles/OfferModal.css";

class OfferModal extends Component {
  render() {
    const {
      pickup_location,
      dropoff_location,
      shipment_attributes,
      pickup_date,
      dropoff_date,
      shipment_type,
      shipment_description,
      _id,
    } = this.props.shipmentobj;
    const pickupdate = pickup_date.slice(0, 10);
    const dropoffdate = dropoff_date.slice(0, 10);
    return (
      <Modal
        size="lg"
        show={this.props.offerModalVisible}
        onHide={this.props.modalVisibility}
        backdrop="static"
        animation="true"
        aria-labelledby="example-modal-sizes-title-lg"
      >
        <Modal.Body>
          <div className="p-3">
            <span className="offer-span">OFFER</span>
            <div className="container">
              <div className="row">
                <div className="col-12">
                  <div className="shadow-sm mt-3 p-5">
                    <span className="offer-description">Description</span>
                    <span className="text-capitalize offer-notes ml-2">
                      {shipment_description}
                    </span>
                    <br />
                    <span className="offer-description">Notes</span>
                    <span className="offer-notes ml-5">
                      {shipment_attributes.notes
                        ? shipment_attributes.notes
                        : "----------------"}
                    </span>
                    <br />
                    <br />
                    {/*map */}
                    <div className="row mb-3">
                      <LocationIcon />
                      <div className="col-6">
                        <span className="offer-description d-block mt-1">
                          {pickup_location.address}
                        </span>
                        <span className="offer-notes d-block">
                          {pickupdate}
                        </span>
                      </div>
                      <div className="col-6">
                        <span className="offer-description d-block mt-1">
                          {dropoff_location.address}
                        </span>
                        <span className="offer-notes d-block">
                          {dropoffdate}
                        </span>
                      </div>
                    </div>
                    <hr />
                    {/*----REMOVAL SHIPMEN TYPE------ */}
                    {shipment_type === "REMOVAL" ? (
                      <RowRemoval
                        data={shipment_attributes}
                        type={shipment_type}
                      />
                    ) : (
                      <RowAttributes
                        data={shipment_attributes}
                        type={shipment_type}
                      />
                    )}

                    <hr />
                    {/* text area */}
                    <div className="form-group mt-4">
                      <textarea
                        className="form-control"
                        rows="5"
                        style={{ border: " 0.5px dashed #777777" }}
                      />
                      <span className="offer-notes d-block">Notes</span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {/* form */}
            <OfferModalForm
              shipment_id={_id}
              modalHide={this.props.modalVisibility}
            />
          </div>
        </Modal.Body>
      </Modal>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    CarrierShipments: state.carrierReducer.Carrier_Shipments,
  };
};
export default connect(mapStateToProps, null)(OfferModal);

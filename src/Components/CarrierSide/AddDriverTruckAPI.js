import React, { Component } from "react";
import {
  getCarrier,
  getCarrierUserId,
  getCarrierObj,
} from "../../LocalStorage/Auth";

export default class AddDriverTruckAPI extends Component {
  //--------------INPUT LABEL------------------------
  renderInput(labelTitle, fieldname) {
    return (
      <div className="form-group col-3">
        <label className="offer-notes font-weight-bold">{labelTitle}</label>
        <input
          type="text"
          className="form--control"
          name={fieldname}
          value={this.state.fieldname}
          onChange={this.changeHandler}
        />
      </div>
    );
  }
  changeHandler = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };
  //------------ADD TRUCK AND DRIVE TO SHIPMENT-------------
  formSubmit = (e) => {
    e.preventDefault();
    //IF CARRIER NOT SELECTED THE DIRVER OR TRUCK
    if (this.state.truck === "" || this.state.driver === "") {
      alert("Select Driver and Truck");
      return null;
    }
    //FIND DRIVER FOR ID
    const Driver = this.state.allDrivers.find((driver) => {
      return driver.driver_name === this.state.driver;
    });
    //FIND TRUCK FOR ID
    const Truck = this.state.allTrucks.find((truck) => {
      return truck.vehicle_name === this.state.truck;
    });
    //CHECK SPLIT ROUTE
    if (this.state.shipmentobj.split_route === true) {
      this.AddTruckDriverSimpleShipment(Driver, Truck);
    } else {
      this.AddTruckDriverSplitShipment(Driver, Truck);
    }
  };
  //-----------SIMPLE SHIPMENT-----------------
  AddTruckDriverSimpleShipment = (Driver, Truck) => {
    const token = getCarrier();
    const requestbody = {
      truck_id: Driver._id,
      driver_id: Truck._id,
    };
    const shipment = {
      id: this.state.shipmentid,
    };
    const config = {
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
      },
    };
    this.props.addDriverTruck(
      requestbody,
      config,
      shipment,
      this.addSuccess,
      this.addFailure
    );
  };
  //------------SPLIT SHIPMENT-----------------
  AddTruckDriverSplitShipment = (Driver, Truck) => {
    const token = getCarrier();
    const requestbody = {
      truck_id: Driver._id,
      driver_id: Truck._id,
    };
    const shipment = {
      id: this.state.shipmentid,
    };
    const config = {
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
      },
    };
    this.props.addDriverTruckSplit(
      requestbody,
      config,
      shipment,
      this.addSuccess,
      this.addFailure
    );
  };

  addSuccess = () => {
    console.log("succes add");
    alert("Added truck and carrier Successfully");
    this.setState({ addedSuccess: true });
  };
  addFailure = () => {
    console.log("add fails");
    alert("Added truck and carrier fails");
  };

  //-------------------GET ALL VEHICALS------------------------
  GetAllVehicals = () => {
    const token = getCarrier();
    const { id } = getCarrierUserId();
    const requestBody = {
      carrierid: id,
    };
    const config = {
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
      },
    };
    this.props.getAllVehicals(
      requestBody,
      config,
      this.GetVehicalsSucess,
      this.GetVehicalsFailure
    );
  };
  GetVehicalsSucess = () => {
    console.log("get vehicals success");
    this.setState({ allTrucks: this.props.Trucks });
  };
  GetVehicalsFailure = () => {
    console.log("get vehicals success");
  };
  //--------------------GET ALL DRIVERS----------------------
  GetAllDrivers = () => {
    const token = getCarrier();
    const { id } = getCarrierUserId();
    const requestBody = {
      carrierid: id,
    };
    const config = {
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
      },
    };
    this.props.getAllDrivers(
      requestBody,
      config,
      this.GetDriversSucess,
      this.GetDriversFailure
    );
  };
  GetDriversSucess = () => {
    console.log("get drivers success");
    this.setState({ allDrivers: this.props.Drivers, loading: false });
  };
  GetDriversFailure = () => {
    console.log("get drivers fails");
  };
  //--------------GET SHIPMENT BY ID-----------------
  GetShipmentById = () => {
    let shipment;
    const { id: carrierid } = getCarrierUserId();
    if (this.props.authToken === "") {
      //get user details from async storage
      const token = getCarrier();
      const carrier = getCarrierObj();
      this.props.addCarrierDetails(token, carrier);
    }

    if (
      (shipment = this.props.Carrier_AllShipment.find(
        (el) => el._id === this.state.shipmentid
      ))
    ) {
      console.log("find shipmet from reducer", shipment);
      this.setState({ shipmentobj: shipment, loading: false });
    } else {
      //call action creator here

      const config = {
        headers: {
          Authorization: `Bearer ${getCarrier()}`,
          "Content-Type": "application/json",
        },
      };
      this.props.getShipmentbyId(
        this.state.shipmentid,
        carrierid,
        config,
        this.onSuccessCallback
      );
    }
  };
  onSuccessCallback = (shipment) => {
    console.log("successfuly get shipment from api", shipment);
    this.setState({ shipmentobj: shipment, loading: false });
  };
}

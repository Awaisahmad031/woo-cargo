import React, { Component } from "react";
import { getCarrier, getCarrierUserId } from "../../LocalStorage/Auth";

export default class ApiCallCarrierShipment extends Component {
  //RENDER INPUT FIELDS
  renderInput = (labelTitle, fieldName) => {
    return (
      <React.Fragment>
        <label className="filter-title">{labelTitle}</label>
        <input
          type="text"
          className="form-control form-control-sm"
          name={fieldName}
          value={this.state.fieldName}
          onChange={this.changeHandle}
          required
        />
      </React.Fragment>
    );
  };
  //--------------------Api call--------------------
  requestToLocalStorage = () => {
    const token = getCarrier();
    const { id } = getCarrierUserId();
    const requestBody = {
      carrierid: id,
    };
    const config = {
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
      },
    };
    this.props.getCarrierShipments(
      requestBody,
      config,
      this.GetShipmentsSucess,
      this.GetShipmentsFailure
    );
  };
  GetShipmentsFailure = () => {
    console.log("Fail get shipments");
  };
  GetShipmentsSucess = () => {
    console.log("Sucessfully get shipment");
    const record = this.props.carrier_AllShipments.filter((shipment) => {
      return shipment.offer_accepted === undefined;
    });
    this.setState({ record, loading: false });
  };
}

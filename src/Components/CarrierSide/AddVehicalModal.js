import React, { Component } from "react";
import { connect } from "react-redux";
import ModalBody from "react-bootstrap/ModalBody";
import {
  AddVehicalCarrierSide,
  EditVehical,
  DeleteVehical,
} from "../../Actions/actioncreators";
import { getCarrier, getCarrierUserId } from "../../LocalStorage/Auth";

class AddVehicalModal extends Component {
  state = {
    token: getCarrier(),
    data: { ...this.returnState() },
  };

  returnState() {
    if (this.props.title === "EditOrDeleteVehical") {
      return this.props.data;
    } else
      return {
        vehicle_name: "",
        registeration_num: "",
        vehicle_model: "",
        vehicle_type: "",
        vehicle_capacity: "",
        vehicle_weight: "",
      };
  }

  changeHandle = ({ target }) => {
    const data = { ...this.state.data };
    data[target.name] = target.value;
    this.setState({ data });
  };
  //INPUT FIELDS OF MODAL
  renderInput(labelTitle, fieldname, typefield) {
    const { data } = this.state;
    return (
      <React.Fragment>
        <label className="numbercolor">{labelTitle}</label>
        <input
          type={typefield}
          className="form-control"
          name={fieldname}
          value={data[fieldname]}
          onChange={this.changeHandle}
          required
        />
      </React.Fragment>
    );
  }
  //-----------ADD VEHICAL----------
  addVehical = () => {
    const { id } = getCarrierUserId();
    const { data } = this.state;
    const requestBody = {
      vehicle_name: data.vehicle_name,
      registeration_num: data.registeration_num,
      vehicle_model: data.vehicle_model,
      vehicle_type: data.vehicle_type,
      vehicle_capacity: data.vehicle_capacity,
      vehicle_weight: data.vehicle_weight,
      carrier_id: id,
    };
    const config = {
      headers: {
        Authorization: `Bearer ${this.state.token}`,
        "Content-Type": "application/json",
      },
    };
    this.props.addVehical(
      requestBody,
      config,
      this.AddVehicalSucess,
      this.AddVehicalFailure
    );
  };
  AddVehicalSucess = () => {
    alert("Vehical added successfully");
  };
  AddVehicalFailure = () => {
    alert("DUPLICATE REGISTRATION NUMBER ERROR");
  };
  //-------------EDIT VEHICAL-----------
  editVehical = () => {
    const vehicalId = this.props.data._id;
    const { data } = this.state;
    const requestBody = {
      vehicle_name: data.vehicle_name,
      registeration_num: data.registeration_num,
      vehicle_model: data.vehicle_model,
      vehicle_type: data.vehicle_type,
      vehicle_capacity: data.vehicle_capacity,
      vehicle_weight: data.vehicle_weight,
    };
    const config = {
      headers: {
        Authorization: `Bearer ${this.state.token}`,
        "Content-Type": "application/json",
      },
    };
    this.props.editVehicalData(
      requestBody,
      config,
      vehicalId,
      this.editDeleteSuccess,
      this.editDeleteFail
    );
  };
  //-------------DELETE VEHICAL---------
  deleteVehical = () => {
    const vehicalId = this.props.data._id;
    const config = {
      headers: {
        Authorization: `Bearer ${this.state.token}`,
        "Content-Type": "application/json",
      },
    };
    this.props.deleteVehicalData(
      config,
      vehicalId,
      this.editDeleteSuccess,
      this.editDeleteFail
    );
  };
  editDeleteSuccess = () => {
    alert("Successfully DONE");
  };
  editDeleteFail = () => {
    alert("FAILS the PROCESS");
  };
  render() {
    return (
      <ModalBody>
        <div className="p-4">
          <div className="container">
            <span className="vehical-modal-title">Truck Data</span>
            <p className="vehical-modal-para mt-4">
              Use this page to update your Truck information
            </p>
            <div>
              <div className="form-row">
                <div className="form-group col-md-8">
                  {this.renderInput("VEHICLE NAME", "vehicle_name", "text")}
                </div>
                <div className="form-group col-md-4">
                  {this.renderInput(
                    "REGISTRATION NO.",
                    "registeration_num",
                    "text"
                  )}
                </div>
              </div>
              <div className="form-row">
                <div className="form-group col-md-4">
                  {this.renderInput("VEHICAL MODAL", "vehicle_model", "text")}
                </div>
                <div className="form-group offset-md-4 col-md-4">
                  {this.renderInput("TYPE", "vehicle_type", "text")}
                </div>
              </div>
              <div className="form-row">
                <div className="form-group col-md-4">
                  {this.renderInput(
                    " CAPACITY (in palattes)",
                    "vehicle_capacity",
                    "number"
                  )}
                </div>
                <div className="form-group offset-md-4 col-md-4">
                  {this.renderInput(
                    "WEIGHT (in tones)",
                    "vehicle_weight",
                    "number"
                  )}
                </div>
              </div>
              {this.props.title === "EditOrDeleteVehical" ? (
                <div className="row">
                  <div className="col-md-4">
                    <button
                      onClick={this.editVehical}
                      className="btn btn-success btn-block"
                    >
                      Update
                    </button>
                  </div>
                  <div className="col-md-4">
                    <button
                      className="btn btn-danger btn-block"
                      onClick={this.deleteVehical}
                    >
                      Delete
                    </button>
                  </div>
                  <div className="col-md-4">
                    <button
                      onClick={this.props.handleModal}
                      className="btn btn-primary btn-block"
                    >
                      Close
                    </button>
                  </div>
                </div>
              ) : (
                <div className="row">
                  <div className="col-md-4">
                    <button
                      onClick={this.addVehical}
                      className="btn btn-primary btn-block"
                    >
                      Save
                    </button>
                  </div>
                  <div className="col-md-4 offset-md-4">
                    <button
                      className="btn btn-danger btn-block"
                      onClick={this.props.handleModal}
                    >
                      Close
                    </button>
                  </div>
                </div>
              )}
            </div>
          </div>
        </div>
      </ModalBody>
    );
  }
}
const mapDispatchToProos = (dispatch) => {
  return {
    addVehical: (requestBody, config, AddVehicalSucess, AddVehicalFailure) =>
      dispatch(
        AddVehicalCarrierSide(
          requestBody,
          config,
          AddVehicalSucess,
          AddVehicalFailure
        )
      ),
    editVehicalData: (requestBody, config, vehicalId, editSuccess, editFail) =>
      dispatch(
        EditVehical(requestBody, config, vehicalId, editSuccess, editFail)
      ),
    deleteVehicalData: (config, vehicalId, editSuccess, editFail) =>
      dispatch(DeleteVehical(config, vehicalId, editSuccess, editFail)),
  };
};
export default connect(null, mapDispatchToProos)(AddVehicalModal);

import React, { Component } from "react";
import { connect } from "react-redux";
import { CarrierOfferSend } from "../../Actions/actioncreators";
import { getCarrierUserId, getCarrier } from "../../LocalStorage/Auth";
class OfferModalForm extends Component {
  state = {
    price: "",
    type: "",
    pickupdate: "",
    pickuptime: "",
    deliverydate: "",
    deliverytime: "",
    offerSend: false,
  };
  changeHandle = (e) => {
    this.setState({ [e.target.name]: e.target.value });
  };
  SendOffer = (e) => {
    e.preventDefault();
    const token = getCarrier();
    const { id: CarrierID } = getCarrierUserId();
    const ShipmentID = this.props.shipment_id;
    const requestbody = {
      carrier: CarrierID,
      cost: this.state.price,
      date_pickup: this.state.pickupdate,
      date_dropoff: this.state.deliverydate,
      shipment_id: ShipmentID,
    };
    const config = {
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
      },
    };
    this.props.SendOffer(
      requestbody,
      config,
      this.offerSendSuccess,
      this.offerSendFailure
    );
  };
  offerSendSuccess = () => {
    alert("Offer Send Successfuly");
    this.setState({ offerSend: true });
  };
  offerSendFailure = () => {
    alert("Offer Send Fail");
  };
  renderInput(labelTitle, fieldname, typename) {
    return (
      <React.Fragment>
        <label className="flabelStyle text-uppercase">{labelTitle}</label>
        <input
          type={typename}
          className="form-control"
          name={fieldname}
          value={this.state.fieldname}
          onChange={this.changeHandle}
          required
        />
      </React.Fragment>
    );
  }
  render() {
    return (
      <div className="mt-5">
        <span className="font15">Please make your offer</span>
        <form onSubmit={this.SendOffer}>
          <div className="form-row mt-3">
            <div className="form-group col-md-4">
              {this.renderInput("Price", "price", "number")}
            </div>
            <div className="form-group col-md-4">
              {this.renderInput("Type", "type", "text")}
            </div>
          </div>
          <div className="form-row mt-3">
            <div className="form-group col-md-3">
              {this.renderInput("Pickup Date", "pickupdate", "date")}
            </div>
            <div className="form-group col-md-3">
              {this.renderInput("Pickup Time", "pickuptime", "time")}
            </div>
            <div className="form-group col-md-3">
              {this.renderInput("Delivery Date", "deliverydate", "date")}
            </div>
            <div className="form-group col-md-3">
              {this.renderInput("Delivery Time", "deliverytime", "time")}
            </div>
          </div>
          <div className="form-row mt-3">
            <div className="form-group col-3">
              <button className="btn btn-primary btn-block" type="submit">
                Send Offer
              </button>
            </div>
            <div className="form-group offset-md-6 col-3">
              <button
                className="btn btn-danger btn-block"
                onClick={this.props.modalHide}
              >
                Close
              </button>
            </div>
          </div>
        </form>
      </div>
    );
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    SendOffer: (requestbody, config, offerSendSuccess, offerSendFailure) => {
      dispatch(
        CarrierOfferSend(
          requestbody,
          config,
          offerSendSuccess,
          offerSendFailure
        )
      );
    },
  };
};
export default connect(null, mapDispatchToProps)(OfferModalForm);

import { Component } from "react";
import { getCarrier, getCarrierUserId } from "../../LocalStorage/Auth";

export default class VehicalApiCall extends Component {
  //----------------Request to local storage after refreshing the page-------------
  requestToLocalStorage = () => {
    const token = getCarrier();
    const { id } = getCarrierUserId();
    const requestBody = {
      carrierid: id,
    };
    const config = {
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
      },
    };
    this.props.getAllVehicals(
      requestBody,
      config,
      this.GetVehicalsSucess,
      this.GetVehicalsFailure
    );
  };
  GetVehicalsSucess = () => {
    console.log("Vehical fetch success");
    const record = this.props.carrier_AllVehicals.reverse();
    this.setState({ record, loading: false });
  };
  GetVehicalsFailure = () => {
    console.log("Vehical fetch Fail");
  };
}

import React, { Component } from "react";
import ModalBody from "react-bootstrap/ModalBody";
import { connect } from "react-redux";
import {
  AddDriverCarrierSide,
  EditDriver,
  DeleteDriver,
} from "../../Actions/actioncreators";
import { getCarrierUserId, getCarrier } from "../../LocalStorage/Auth";

class AddDriverModal extends Component {
  state = {
    token: getCarrier(),
    data: { ...this.returnState() },
    loadingButton: false,
    addDriverError: false,
  };
  returnState() {
    if (this.props.title === "EditOrDeleteDriver") {
      return this.props.data;
    } else
      return {
        driver_name: "",
        driver_email: "",
        driver_mobilenum: "",
        driver_password: "",
      };
  }

  changeHandle = ({ target }) => {
    if (this.state.addDriverError === true) {
      this.setState({ addDriverError: false });
    }
    const data = { ...this.state.data };
    data[target.name] = target.value;
    this.setState({ data });
  };
  //-------------------INPUT FIELD------------------------
  renderInput(labelTitle, fieldname, typefield) {
    const { data } = this.state;
    return (
      <React.Fragment>
        <label className="numbercolor">{labelTitle}</label>
        <input
          type={typefield}
          className="form-control"
          name={fieldname}
          value={data[fieldname]}
          onChange={this.changeHandle}
          required
        />
      </React.Fragment>
    );
  }
  loadingButton = () => {
    this.setState({ loadingButton: true });
  };
  //------------------ADD DRIVER API-----------------
  addDriverForm = () => {
    this.loadingButton();
    const { data } = this.state;
    const { id: carrierID } = getCarrierUserId();
    const requestBody = {
      driver_name: data.driver_name,
      driver_mobilenum: data.driver_mobilenum,
      driver_email: data.driver_email,
      carrier_id: carrierID,
      driver_password: data.driver_password,
    };
    const config = {
      headers: {
        Authorization: `Bearer ${this.state.token}`,
        "Content-Type": "application/json",
      },
    };
    this.props.addDriver(
      requestBody,
      config,
      this.AddDriversSucess,
      this.AddDriversFailure
    );
  };
  AddDriversSucess = () => {
    this.setState({ loadingButton: false });
    alert("Driver added successfully ");
  };

  AddDriversFailure = () => {
    this.setState({ loadingButton: false, addDriverError: true });
  };
  //---------------EDIT DRIVER API----------------
  editDriver = () => {
    const { data } = this.state;
    const driverId = this.props.data._id;
    const config = {
      headers: {
        Authorization: `Bearer ${this.state.token}`,
        "Content-Type": "application/json",
      },
    };
    const requestBody = {
      driver_name: data.driver_name,
      driver_mobilenum: data.driver_mobilenum,
      driver_email: data.driver_email,
    };
    this.props.editDriverData(
      requestBody,
      config,
      driverId,
      this.editDeleteSuccess,
      this.editDeleteFail
    );
  };
  //---------------DELETE DRIVER API----------------
  deleteDriver = () => {
    const driverId = this.props.data._id;
    const config = {
      headers: {
        Authorization: `Bearer ${this.state.token}`,
        "Content-Type": "application/json",
      },
    };
    this.props.deleteDriverData(
      config,
      driverId,
      this.editDeleteSuccess,
      this.editDeleteFail
    );
  };
  editDeleteSuccess = () => {
    alert("Successfully DONE");
  };
  editDeleteFail = () => {
    alert("FAILS the PROCESS");
  };
  render() {
    const { loadingButton, addDriverError } = this.state;
    return (
      <ModalBody>
        <div className="p-4">
          <div className="container">
            <span className="vehical-modal-title">Driver Data</span>
            {/*---------DRIVER EMAIL ERROR START-------- */}
            {addDriverError && (
              <div
                className="alert alert-danger float-right"
                style={{ fontSize: "14px" }}
              >
                Current Driver Email is already taken.Try another one
              </div>
            )}
            {/*---------DRIVER EMAIL ERROR END-------- */}
            <p className="vehical-modal-para mt-4">
              Use this page to update your Driver information
            </p>
            <div>
              <div className="form-row">
                <div className="form-group col-md-4">
                  {this.renderInput("NAME", "driver_name", "text")}
                </div>
                <div className="form-group col-md-4">
                  {this.renderInput("EMAIL", "driver_email", "email")}
                </div>
              </div>
              <div className="form-row">
                {this.props.title === "EditOrDeleteDriver" ? null : (
                  <div className="form-group col-md-4">
                    {this.renderInput(
                      "PASSWORD",
                      "driver_password",
                      "password"
                    )}
                  </div>
                )}

                <div className="form-group col-md-4">
                  {this.renderInput("MOBILE NO.", "driver_mobilenum", "number")}
                </div>
              </div>
              {this.props.title === "EditOrDeleteDriver" ? (
                <div className="row">
                  <div className="col-md-4">
                    <button
                      onClick={this.editDriver}
                      className="btn btn-success btn-block"
                    >
                      Update
                    </button>
                  </div>
                  <div className="col-md-4">
                    <button
                      className="btn btn-danger btn-block"
                      onClick={this.deleteDriver}
                    >
                      Delete
                    </button>
                  </div>
                  <div className="col-md-4">
                    <button
                      onClick={this.props.handleModal}
                      className="btn btn-primary btn-block"
                    >
                      Close
                    </button>
                  </div>
                </div>
              ) : (
                <div className="row">
                  <div className="col-md-4">
                    <button
                      onClick={this.addDriverForm}
                      disabled={loadingButton}
                      className="btn btn-primary btn-block"
                    >
                      {loadingButton ? (
                        <span>Loading ...</span>
                      ) : (
                        <span>Save</span>
                      )}
                    </button>
                  </div>
                  <div className="col-md-4 offset-md-4">
                    <button
                      className="btn btn-danger btn-block"
                      onClick={this.props.handleModal}
                    >
                      Close
                    </button>
                  </div>
                </div>
              )}
            </div>
          </div>
        </div>
      </ModalBody>
    );
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    addDriver: (requestBody, config, AddDriversSucess, AddDriversFailure) =>
      dispatch(
        AddDriverCarrierSide(
          requestBody,
          config,
          AddDriversSucess,
          AddDriversFailure
        )
      ),
    editDriverData: (requestBody, config, driverId, editSuccess, editFail) =>
      dispatch(
        EditDriver(requestBody, config, driverId, editSuccess, editFail)
      ),
    deleteDriverData: (config, driverId, editSuccess, editFail) =>
      dispatch(DeleteDriver(config, driverId, editSuccess, editFail)),
  };
};
export default connect(null, mapDispatchToProps)(AddDriverModal);

import React from "react";
import RemovalsformValidation from "./RemovalsformValidation";
import Switch from "react-switch";
import { AddShipmentCustomerSide } from "../../../Actions/actioncreators";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import "../../../Styles/CustomerHome.css";

class Removals extends RemovalsformValidation {
  state = {
    shipment_type: this.props.shipmentInfo.shipment_type,
    shipment_title: this.props.shipmentInfo.shipment_title,
    error_shipmentType: false,
    data: {
      pickuplocation: "",
      pickupdate: "",
      pickuptime: "",
      deliverylocation: "",
      deliverydate: "",
      deliverytime: "",
      description_dateTime: "",
      notes: "",
      description: "",
      fromfloor: "",
      tofloor: "",
      roomNo: "",
    },
    fromfloorLift: false,
    tofloorLift: false,
    packing: false,
    assembly: false,
    disassembly: false,
    Delivery_Flexible_Time: false,
    Pickup_Flexible_Time: false,
    error: {},
  };
  render() {
    return (
      <div>
        {/*-------LOCATION AND TIME--------- */}
        <div className="container mt-5">
          <div
            className="container shadow-sm p-4 pb-5"
            style={{ background: "#ffffff" }}
          >
            <p className="pt-4" style={{ color: "#8798AD" }}>
              LOCATION & TIME
            </p>
            <p>Enter the location the date and time for pickup and delivery.</p>
            <div className="form-row">
              <div className="form-group col-md-4">
                {this.renderInput("PICKUP LOCATION", "pickuplocation")}
              </div>
              <div className="form-group col-md-3">
                {this.renderInput("DATE", "pickupdate", "date")}
              </div>
              <div className="form-group col-md-3">
                {this.renderInput("TIME", "pickuptime", "time")}
              </div>
              <div className="form-group col-md-2">
                <label className="newShipmentLabel">FLEXIBLE TIME</label>
                <br />
                <Switch
                  name="Pickup_Flexible_Time"
                  onChange={this.PickupFlexibleTime}
                  checked={this.state.Pickup_Flexible_Time}
                />
              </div>
            </div>
            <div className="form-row">
              <div className="form-group col-md-4">
                {this.renderInput("DELIVERY LOCATION", "deliverylocation")}
              </div>
              <div className="form-group col-md-3">
                {this.renderInput("DATE", "deliverydate", "date")}
              </div>
              <div className="form-group col-md-3">
                {this.renderInput("TIME", "deliverytime", "time")}
              </div>
              <div className="form-group col-md-2">
                <label className="newShipmentLabel">FLEXIBLE TIME</label>
                <br />
                <Switch
                  name="Delivery_Flexible_Time"
                  onChange={this.DeliveryFlexibleTime}
                  checked={this.state.Delivery_Flexible_Time}
                />
              </div>
            </div>
            <div className="form-row">
              <div className="form-group col-md-7">
                {this.renderInput("DESCRIPTION", "description_dateTime")}
              </div>
              <div className="form-group col">
                {this.renderNotes("NOTES", "notes")}
              </div>
            </div>
          </div>
        </div>
        {/*-----------------WEIGHT AND DIMENSION */}
        <div className="container my-5">
          <div
            className="container shadow-sm p-4 pb-5"
            style={{ background: "#ffffff" }}
          >
            <p className="pt-4" style={{ color: "#8798AD" }}>
              DIMENSION AND WEIGHT
            </p>
            <p>Enter the dimensions and weight of the shipment.</p>

            <div className="form-row">
              <div className="form-group col-md-12">
                {this.renderNotes("DESCRIPTION", "description")}
              </div>
            </div>
            <div className="form-row">
              <div className="form-group col-md-2">
                {this.renderInput("FROM FLOOR", "fromfloor", "number")}
              </div>
              <div className="form-group col-md-2">
                <label className="newShipmentLabel">WITH LIFT</label>
                <br />
                <Switch
                  name="fromfloorLift"
                  onChange={this.fromFloorLift}
                  checked={this.state.fromfloorLift}
                />
              </div>
              <div className="form-group offset-md-1 col-md-2">
                {this.renderInput("TO FLOOR", "tofloor", "number")}
              </div>
              <div className="form-group col-md-2">
                <label className="newShipmentLabel">WITH LIFT</label>
                <br />
                <Switch
                  name="tofloorLift"
                  onChange={this.toFloorLift}
                  checked={this.state.tofloorLift}
                />
              </div>
            </div>
            <div className="form-row">
              <div className="form-group col-md-2">
                {this.renderInput("ROOM NUMBER", "roomNo", "number")}
              </div>
              <div className="form-group offset-md-1 col-md-2">
                <label className="newShipmentLabel">PACKING</label>
                <br />
                <Switch
                  name="packing"
                  onChange={this.handlePacking}
                  checked={this.state.packing}
                />
              </div>
              <div className="form-group col-md-2">
                <label className="newShipmentLabel">DISASSEMBLY</label>
                <br />
                <Switch
                  name="disassembly"
                  onChange={this.handleDisassembly}
                  checked={this.state.disassembly}
                />
              </div>
              <div className="form-group col-md-2">
                <label className="newShipmentLabel">ASSEMBLY</label>
                <br />
                <Switch
                  name="assembly"
                  onChange={this.handleAssembly}
                  checked={this.state.assembly}
                />
              </div>
            </div>
            <div className="form-row">
              <div className="form-group offset-md-10 col-md-2">
                <button className="btn btn-success mt-5" type="submit">
                  Add Packaging
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    customer_id: state.customerReducer.Customer._id,
    auth_Token: state.customerReducer.auth_Token,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    AddShipment: (
      requestBody,
      config,
      AddShipmentSucessCustomer,
      AddShipmentFailureCustomer
    ) => {
      dispatch(
        AddShipmentCustomerSide(
          requestBody,
          config,
          AddShipmentSucessCustomer,
          AddShipmentFailureCustomer
        )
      );
    },
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(Removals));

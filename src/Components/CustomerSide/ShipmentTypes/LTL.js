import React from "react";
import LTLformValidation from "./LTLformValidation";
import Switch from "react-switch";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";
import UploadPics from "./UploadPics";
import Location from "./Location";
import { AddShipmentCustomerSide } from "../../../Actions/actioncreators";
let Picslocation = [];
class LTL extends LTLformValidation {
  state = {
    shipment_type: this.props.shipmentInfo.shipment_type,
    shipment_title: this.props.shipmentInfo.shipment_title,
    error_shipmentType: false,
    data: {
      pickupdate: "",
      pickuptime: "",
      deliverydate: "",
      deliverytime: "",
      description_dateTime: "",
      notes: "",
      packaging: "",
      dimensions: "",
      lenght: "",
      width: "",
      height: "",
      type: "",
      weight: "",
      quantity: "",
      description_Packaging: "",
    },
    Delivery_Flexible_Time: false,
    Pickup_Flexible_Time: false,
    pickuplocation: "",
    deliverylocation: "",
    Pickup_latlng: {},
    Delivery_latlng: {},
    error: {},
    PicsLocation: [],
  };

  picsHandle = (PicsLocation) => {
    
    this.setState({PicsLocation:[PicsLocation[0].location]});
   
  };
  pickupLocation = (location, lat,lng) => {
    console.log(lat)
    this.setState({ pickuplocation:[location,lat,lng]});
  };
  deliveryLocation = (location, lat,lng) => {
    this.setState({ deliverylocation:[location,lat,lng] });
  };
  render() {
    console.log("LTL", this.state.PicsLocation);
    return (
      <div>
        {/*-------LOCATION AND TIME--------- */}
        <div className="container mt-5">
          <div
            className="container shadow-sm p-4 pb-5"
            style={{ background: "#ffffff" }}
          >
            <p className="pt-4" style={{ color: "#8798AD" }}>
              LOCATION & TIME
            </p>
            <p>Enter the location the date and time for pickup and delivery.</p>
            <div className="form-row">
              <div className="form-group col-md-4">
                <Location
                  labeltitle="PICKUP LOCATION"
                  pickuplocation={this.pickupLocation}
                />
              </div>
              <div className="form-group col-md-3">
                {this.renderInput("DATE", "pickupdate", "date")}
              </div>
              <div className="form-group col-md-3">
                {this.renderInput("TIME", "pickuptime", "time")}
              </div>
              <div className="form-group col-md-2">
                <label
                  className="font-weight-bolder"
                  style={{ color: "#B0BAC9", fontSize: "15px" }}
                >
                  FLEXIBLE TIME
                </label>
                <br />
                <Switch
                  id="Pickup_Flexible_Time"
                  onChange={this.PickupFlexibleTime}
                  checked={this.state.Pickup_Flexible_Time}
                />
              </div>
            </div>
            <div className="form-row">
              <div className="form-group col-md-4">
                <Location
                  labeltitle="DELIVERY LOCATION"
                  deliverylocation={this.deliveryLocation}
                />
              </div>
              <div className="form-group col-md-3">
                {this.renderInput("DATE", "deliverydate", "date")}
              </div>
              <div className="form-group col-md-3">
                {this.renderInput("TIME", "deliverytime", "time")}
              </div>
              <div className="form-group col-md-2">
                <label
                  className="font-weight-bolder"
                  style={{ color: "#B0BAC9", fontSize: "15px" }}
                >
                  FLEXIBLE TIME
                </label>
                <br />
                <Switch
                  id="Delivery_Flexible_Time"
                  onChange={this.DeliveryFlexibleTime}
                  checked={this.state.Delivery_Flexible_Time}
                />
              </div>
            </div>
            <div className="form-row">
              <div className="form-group col-md-7">
                {this.renderInput("DESCRIPTION", "description_dateTime")}
              </div>
              <div className="form-group col">
                {this.renderNotes("NOTES", "notes")}
              </div>
            </div>
          </div>
        </div>
        {/*-----------------WEIGHT AND DIMENSION */}
        <div className="container my-5">
          <div
            className="container shadow-sm p-4 pb-5"
            style={{ background: "#ffffff" }}
          >
            <p className="pt-4" style={{ color: "#8798AD" }}>
              DIMENSION AND WEIGHT
            </p>
            <p>Enter the dimensions and weight of the shipment.</p>

            <div className="form-row">
              <div className="form-group col-md-3">
                {this.renderOptionField("PACKAGING", "packaging", [
                  "true",
                  "false",
                ])}
              </div>
              <div className="form-group col-md-3">
                {this.renderInput("DIMENSION", "dimensions")}
              </div>
            </div>
            <div className="form-row">
              <div className="form-group col-md-2">
                {this.renderInput("LENGTH", "lenght", "number")}
              </div>
              <div className="form-group col-md-2">
                {this.renderInput("WIDTH", "width", "number")}
              </div>
              <div className="form-group col-md-2">
                {this.renderInput("HEIGHT", "height", "number")}
              </div>
              <div className="form-group col-md-2">
                {this.renderInput("Type", "type")}
              </div>
            </div>
            <div className="form-row">
              <div className="form-group col-md-2">
                {this.renderInput("WEIGHT", "weight", "number")}
              </div>
              <div className="form-group col-md-2">
                {this.renderInput("QUANTITY", "quantity", "number")}
              </div>
            </div>
            <div className="form-row">
              <div className="form-group col-md-7">
                {this.renderInput("DESCRIPTION", "description_Packaging")}
              </div>
              <div className="for-group offset-md-3 col-md-2">
                <button className="btn btn-success mt-5 ">Add Packaging</button>
              </div>
            </div>
          </div>
        </div>
        {/*----------------PHOTOS--------- */}
        <div className="container my-5">
          <UploadPics picsHandle={this.picsHandle} />
          <button
            className="btn btn-primary float-right"
            onClick={this.formSubmition}
          >
            Done
          </button>
        </div>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    customer_id: state.customerReducer.Customer._id,
    auth_Token: state.customerReducer.auth_Token,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    AddShipment: (
      requestBody,
      config,
      AddShipmentSucessCustomer,
      AddShipmentFailureCustomer
    ) => {
      dispatch(
        AddShipmentCustomerSide(
          requestBody,
          config,
          AddShipmentSucessCustomer,
          AddShipmentFailureCustomer
        )
      );
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(withRouter(LTL));

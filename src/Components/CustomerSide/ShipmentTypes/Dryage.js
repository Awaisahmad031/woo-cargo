import React from "react";
import DryageformValidation from "./DryageformValidation";
import Switch from "react-switch";
import { AddShipmentCustomerSide } from "../../../Actions/actioncreators";
import { withRouter } from "react-router-dom";
import { connect } from "react-redux";

class Dryage extends DryageformValidation {
  state = {
    shipment_type: this.props.shipmentInfo.shipment_type,
    shipment_title: this.props.shipmentInfo.shipment_title,
    error_shipmentType: false,
    data: {
      pickuplocation: "",
      pickupdate: "",
      pickuptime: "",
      deliverylocation: "",
      deliverydate: "",
      deliverytime: "",
      description: "",
      notes: "",
      packaging: "",
      dimensions: "",
      type: "",
      weight: "",
      quantity: "",
    },
    Delivery_Flexible_Time: false,
    Pickup_Flexible_Time: false,
    error: {},
  };
  render() {
    return (
      <div>
        {/*-------LOCATION AND TIME--------- */}
        <div className="container mt-5">
          <div
            className="container shadow-sm p-4 pb-5"
            style={{ background: "#ffffff" }}
          >
            <p className="pt-4" style={{ color: "#8798AD" }}>
              LOCATION & TIME
            </p>
            <p>Enter the location the date and time for pickup and delivery.</p>
            <div className="form-row">
              <div className="form-group col-md-4">
                {this.renderInput("PICKUP LOCATION", "pickuplocation")}
              </div>
              <div className="form-group col-md-3">
                {this.renderInput("DATE", "pickupdate", "date")}
              </div>
              <div className="form-group col-md-3">
                {this.renderInput("TIME", "pickuptime", "time")}
              </div>
              <div className="form-group col-md-2">
                <label
                  className="font-weight-bolder"
                  style={{ color: "#B0BAC9", fontSize: "15px" }}
                >
                  FLEXIBLE TIME
                </label>
                <br />
                <Switch
                  name="Pickup_Flexible_Time"
                  onChange={this.PickupFlexibleTime}
                  checked={this.state.Pickup_Flexible_Time}
                />
              </div>
            </div>
            <div className="form-row">
              <div className="form-group col-md-4">
                {this.renderInput("DELIVERY LOCATION", "deliverylocation")}
              </div>
              <div className="form-group col-md-3">
                {this.renderInput("DATE", "deliverydate", "date")}
              </div>
              <div className="form-group col-md-3">
                {this.renderInput("TIME", "deliverytime", "time")}
              </div>
              <div className="form-group col-md-2">
                <label
                  className="font-weight-bolder"
                  style={{ color: "#B0BAC9", fontSize: "15px" }}
                >
                  FLEXIBLE TIME
                </label>
                <br />
                <Switch
                  name="Delivery_Flexible_Time"
                  onChange={this.DeliveryFlexibleTime}
                  checked={this.state.Delivery_Flexible_Time}
                />
              </div>
            </div>
            <div className="form-row">
              <div className="form-group col-md-7">
                {this.renderInput("DESCRIPTION", "description")}
              </div>
              <div className="form-group col">
                {this.renderNotes("NOTES", "notes")}
              </div>
            </div>
          </div>
        </div>
        {/*-----------------WEIGHT AND DIMENSION-------------- */}
        <div className="container my-5">
          <div
            className="container shadow-sm p-4 pb-5"
            style={{ background: "#ffffff" }}
          >
            <p className="pt-4" style={{ color: "#8798AD" }}>
              DIMENSION AND WEIGHT
            </p>
            <p>Enter the dimensions and weight of the shipment.</p>

            <div className="form-row">
              <div className="form-group col-md-2">
                {this.renderInput("TYPE OF DRYAGE", "type")}
              </div>
            </div>
            <div className="form-row">
              <div className="form-group col-md-2">
                {this.renderOptionField("PACKAGING", "packaging", [
                  "true",
                  "false",
                ])}
              </div>
              <div className="form-group col-md-2">
                {this.renderInput("DIMENSION", "dimensions")}
              </div>
              <div className="form-group col-md-2">
                {this.renderInput("QUANTITY", "quantity", "number")}
              </div>
            </div>
            <div className="form-row">
              <div className="form-group col-md-2">
                {this.renderInput("WEIGHT", "weight", "number")}
              </div>
              <div className="form-group offset-md-8 col-md-2">
                <button className="btn btn-success mt-5">Add Packaging</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    customer_id: state.customerReducer.Customer._id,
    auth_Token: state.customerReducer.auth_Token,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    AddShipment: (
      requestBody,
      config,
      AddShipmentSucessCustomer,
      AddShipmentFailureCustomer
    ) => {
      dispatch(
        AddShipmentCustomerSide(
          requestBody,
          config,
          AddShipmentSucessCustomer,
          AddShipmentFailureCustomer
        )
      );
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(withRouter(Dryage));

import React, { Component } from "react";
import InputField from "../../Common/InputField";
import Joi from "joi-browser";
import OptionField from "../OptionField";
import { getCustomerUserId, getCustomer } from "../../../LocalStorage/Auth";

class CarformValidation extends Component {
  //-------------FORM VALIDATION SCHEMA
  schema = {
    pickuplocation: Joi.string().required().label("Pickup Location"),
    pickupdate: Joi.string().required().label("Pickup Date"),
    pickuptime: Joi.string().required().label("Pickup Time"),
    deliverylocation: Joi.string().required().label("Delivery Location"),
    deliverydate: Joi.string().required().label("Delivery Date"),
    deliverytime: Joi.string().required().label("Dropoff Time"),
    description_dateTime: Joi.string().required().label("Description"),
    notes: Joi.string().required(),
    dimensions: Joi.string().required().label("Dimensions"),
    lenght: Joi.number().required().label("Lenght"),
    width: Joi.number().required().label("Width"),
    type: Joi.string().required().label("Truck Type"),
    quantity: Joi.number().required().min(0).label("Quantity"),
    weight: Joi.number().required().min(0).label("Weight"),
    height: Joi.number().required().min(0).label("Height"),
    description_Packaging: Joi.string().required().label("Description"),
  };
  changeHandler = ({ target }) => {
    //------ERROR
    const error = { ...this.state.error };
    const errorMassage = this.validateProperty(target);
    if (errorMassage) error[target.name] = errorMassage;
    else delete error[target.name];
    //-----------DATA
    const data = { ...this.state.data };
    data[target.name] = target.value;
    this.setState({ data, error });
  };
  validateProperty = ({ name, value }) => {
    const obj = { [name]: value };
    const schema = { [name]: this.schema[name] };
    const { error } = Joi.validate(obj, schema);
    return error ? error.details[0].message : null;
  };
  validate = () => {
    const result = Joi.validate(this.state.data, this.schema, {
      abortEarly: false,
    });
    if (!result.error) return null;
    const error = {};
    for (let item of result.error.details) error[item.path[0]] = item.message;
    return error;
  };
  formSubmition = (e) => {
    console.log("adding shipment");
    const { data } = this.state;
    console.log("form submit", this.state);
    e.preventDefault();
    // validation of input fields
    const error = this.validate();
    console.log("error", error);
    this.setState({ error: error || {} });
    //if error occur don't call api
    if (error) {
      alert("Please fill all the fields");
      return null;
    }
    //call the api
    const { id } = getCustomerUserId();
    const token = getCustomer();
    const requestBody = {
      customer_id: id,
      shipment_title: this.state.shipment_title,
      pickup_location: {
        type: "Point",
        coordinates: [74.277851, 31.494184],
        address: data.pickuplocation,
      },
      dropoff_location: {
        type: "Point",
        coordinates: [74.277851, 31.494184],
        address: data.deliverylocation,
      },
      shipment_description: data.description_Packaging,
      shipment_attributes: {
        dimensions: data.dimensions,
        quantity: data.quantity,
        lenght: data.lenght,
        width: data.width,
        trucktype: data.type,
        weight: data.weight,
        height: data.height,
        notes: data.notes,
      },
      shipment_type: this.state.shipment_type,
      dropoff_date: data.deliverydate,
      pickup_date: data.pickupdate,
    };
    const config = {
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
      },
    };
    this.props.AddShipment(
      requestBody,
      config,
      this.AddShipmentSucessCustomer,
      this.AddShipmentFailureCustomer
    );
  };
  AddShipmentSucessCustomer = () => {
    this.props.history.push("/Shipments");
    console.log("Add Shipments Success");
  };
  AddShipmentFailureCustomer = () => {
    console.log("Add Shipment Fail");
    alert("Fail to add shipment");
  };

  renderInput(label, fieldname, type = "text") {
    const { data, error } = this.state;
    return (
      <InputField
        labeltitle={label}
        typename={type}
        fieldname={fieldname}
        changeHandler={this.changeHandler}
        value={data[fieldname]}
        error={error[fieldname]}
      />
    );
  }
  renderOptionField(label, name, Option) {
    const { data, error } = this.state;
    return (
      <OptionField
        labelTitle={label}
        name={name}
        Option={Option}
        value={data[name]}
        error={error[name]}
        changeHandler={this.changeHandler}
      />
    );
  }
  renderNotes(labelTitle, name) {
    const { data, error } = this.state;
    return (
      <React.Fragment>
        <label
          className="font-weight-bolder"
          style={{ color: "#B0BAC9", fontSize: "15px" }}
        >
          {labelTitle}
        </label>
        <textarea
          className="form-control"
          name={name}
          value={data[name]}
          error={error[name]}
          onChange={this.changeHandler}
          rows="4"
        />
      </React.Fragment>
    );
  }
  //---------FOR SWITCH TOGGLER----
  PickupFlexibleTime = () => {
    this.setState({ Pickup_Flexible_Time: !this.state.Pickup_Flexible_Time });
  };
  DeliveryFlexibleTime = () => {
    this.setState({
      Delivery_Flexible_Time: !this.state.Delivery_Flexible_Time,
    });
  };
}

export default CarformValidation;

import React, { Component } from "react";
import PlacesAutocomplete, {
  geocodeByAddress,
  getLatLng,
} from "react-places-autocomplete";

export default class Location extends Component {
  constructor(props) {
    super(props);
    this.state = {
       address: "",

      };
  }
  handleChange = (address) => {
    this.setState({ address });
  };
  handleSelect = (address) => {
    const { labeltitle } = this.props;
    geocodeByAddress(address)
      .then((results) => {
        this.setState({address:results[0].formatted_address})
        console.log(results[0])
      labeltitle === "PICKUP LOCATION"
      ? this.props.pickuplocation(results[0].formatted_address,results[0].geometry.location.lat(),results[0].geometry.location.lng())
      : this.props.deliverylocation(results[0].formatted_address,results[0].geometry.location.lat(),results[0].geometry.location.lng())
      })
      .catch((error) => console.error("Error", error));
  };

  render() {
    return (
      <PlacesAutocomplete
        value={this.state.address}
        onChange={this.handleChange}
        onSelect={this.handleSelect}
      >
        {({ getInputProps, suggestions, getSuggestionItemProps, loading }) => (
          <div>
            <label className="gray_text">{this.props.labeltitle}</label>
            <input {...getInputProps({
                className: "form-control",
              })}
            />
            <div className="autocomplete-dropdown-container" style={{position:'absolute',background:'white',zIndex:1000}}>

              {loading && <div>Loading...</div>}
              {suggestions.map((suggestion) => {
                const className = suggestion.active
                  ? "suggestion-item--active"
                  : "suggestion-item";
                // inline style for demonstration purpose
                const style = suggestion.active
                  ? { backgroundColor: "#f4f6fc", cursor: "pointer" }
                  : { backgroundColor: "#ffffff", cursor: "pointer" };
                return (
                  <div
                    {...getSuggestionItemProps(suggestion, {
                      className,
                      style,
                    })}
                  >
                    <span>{suggestion.description}</span>
                  </div>
                );
              })}
            </div>
          </div>
        )}
      </PlacesAutocomplete>
    );
  }
}

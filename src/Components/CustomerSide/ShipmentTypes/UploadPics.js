import React, { useMemo, useState, useEffect } from "react";
import S3FileUpload from "react-s3";
import { useDropzone } from "react-dropzone";
//----------------STYLING START---------------------
const baseStyle = {
  flex: 1,
  display: "flex",
  flexDirection: "column",
  alignItems: "center",
  padding: "20px",
  borderWidth: 2,
  borderRadius: 2,
  borderColor: "#E0E7FF",
  borderStyle: "dashed",
  backgroundColor: "#f4f6fc",
  color: "#bdbdbd",
  outline: "none",
  transition: "border .24s ease-in-out",
};

const activeStyle = {
  borderColor: "#2196f3",
};

const acceptStyle = {
  borderColor: "#00e676",
};

const rejectStyle = {
  borderColor: "#ff1744",
};
const thumbsContainer = {
  display: "flex",
  flexDirection: "row",
  flexWrap: "wrap",
  marginTop: 16,
};

const thumb = {
  display: "inline-flex",
  borderRadius: 2,
  border: "1px solid #eaeaea",
  marginBottom: 8,
  marginRight: 8,
  width: "auto",
  height: 200,
  padding: 4,
  boxSizing: "border-box",
};

const thumbInner = {
  display: "flex",
  minWidth: 0,
  overflow: "hidden",
};

const img = {
  display: "block",
  width: "auto",
  height: "100%",
};
//----------------STYLING END---------------------//
//----------------UPLOAD PICS CONFIG-------------//
const config = {
  bucketName: "woocargostorage",
  dirName: "shipment-images",
  region: "eu-north-1",
  accessKeyId: "AKIAYKHVIMIGXACW5PC5",
  secretAccessKey: "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",
};
export default function UploadPics({ picsHandle }) {
  const [files, setFiles] = useState([]);
  const [uploadedButton, setUploadedButton] = useState(false);
  const [uploading, setUploading] = useState(false);
  const {
    getRootProps,
    getInputProps,
    isDragActive,
    isDragAccept,
    isDragReject,
    open,
  } = useDropzone({
    accept: "image/*",
    noClick: true,
    noKeyboard: true,
    onDrop: (acceptedFiles) => {
      setFiles(
        acceptedFiles.map((file) =>
          Object.assign(file, {
            preview: URL.createObjectURL(file),
          })
        )
      );
      setUploadedButton(false);
    },
  });
  const thumbs = files.map((file) => (
    <div style={thumb} key={file.name}>
      <div style={thumbInner}>
        <img src={file.preview} style={img} />
      </div>
    </div>
  ));

  useEffect(
    () => () => {
      // Make sure to revoke the data uris to avoid memory leaks
      files.forEach((file) => URL.revokeObjectURL(file.preview));
    },
    [files]
  );
  const uploadPhotos = () => {
    let photos = [];
    let cunter = 0;
    //-----WHEN PHOTO FILE IS EMPATY-----
    if (files.length === 0) {
      return alert("Please first Select the Photos");
    }
    setUploading(true);
    files.forEach((file) => {
      S3FileUpload.uploadFile(file, config)
        .then((data) => {
          console.log("Response", data);
          photos.push(data);
          cunter++;
          console.log("cunter", cunter);
          if (files.length === cunter) {
            console.log("picshadle", photos);
            setUploading(false);
            setUploadedButton(true);
            return picsHandle(photos);
          }
        })
        .catch((err) => {
          setUploading(false);
          alert(err);
        });
    });
  };
  const style = useMemo(
    () => ({
      ...baseStyle,
      ...(isDragActive ? activeStyle : {}),
      ...(isDragAccept ? acceptStyle : {}),
      ...(isDragReject ? rejectStyle : {}),
    }),
    [isDragActive, isDragReject]
  );
  return (
    <div
      className="container shadow-sm p-4 pb-5"
      style={{ background: "#ffffff" }}
    >
      <p className="pt-2" style={{ color: "#8798AD" }}>
        PHOTOS OF SHIPMENT
      </p>
      <p>Drag or select the shipment photos</p>
      <div className="container">
        <div {...getRootProps({ style })}>
          <input {...getInputProps()} />
          <p>Drag a photo here</p>
          <p>Or</p>
          <button type="button" className="btn btn-primary" onClick={open}>
            Select Photos
          </button>
        </div>
        <aside style={thumbsContainer}>{thumbs}</aside>
        <aside>
          {uploadedButton ? (
            <button className="btn btn-success disabled">Uploaded</button>
          ) : uploading ? (
            <button className="btn btn-primary disabled">Uploading ...</button>
          ) : (
            <button className="btn btn-success" onClick={uploadPhotos}>
              Click to Save Photos
            </button>
          )}
        </aside>
      </div>
    </div>
  );
}

import React, { Component } from "react";
import "../../Styles/DetailCusShip.css";

export default class CarrierPickupDel extends Component {
  render() {
    return (
      <div
        className="container p-4 shadow-sm"
        style={{ backgroundColor: "#ffffff", minHeight: "35vh" }}
      >
        <div className="row">
          <span className="ml-2  fontsize" style={{ color: "#0000FF" }}>
            {this.props.title}
          </span>
          <button className="mybtn-blue-secondary ml-auto mr-5 fontsize">
            Chat
          </button>
        </div>
        <hr />

        <div className="body">
          <span className="fontbold font-size">Carrier</span>
          <br />
          <span className="text-muted d-block font-size">Woocargo ltd</span>
          <span className="text-muted d-block font-size">address</span>
          <span className="text-muted d-block-inline mr-5 font-size">Tel.</span>
          <span className="text-muted font-size">Mob.</span>
          <br />
          <hr />
        </div>
        <div className="row">
          <div className="col-6">
            <span className="fontbold d-block font-size">Truck</span>
            <span className="text-muted d-block font-size">--- </span>
            <span className="fontbold d-block font-size">Type</span>
            <span className="text-muted d-block font-size">---</span>
          </div>
          <div className="col-6">
            <span className="fontbold d-block font-size">Driver</span>
            <span className="text-muted d-block font-size">----</span>
            <span className="fontbold d-block font-size">Mobile Phone</span>
            <span className="text-muted d-block font-size">---</span>
          </div>
        </div>
      </div>
    );
  }
}

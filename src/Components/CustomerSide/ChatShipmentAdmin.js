import React, { Component } from "react";
import getAllChatRooms from "../../Services/getallchatrooms";
import { getCustomer } from "../../LocalStorage/Auth";
import "../../Styles/Message.css";

export default class ChatShipmentAdmin extends Component {
  state = {
    allchatrooms: [],
  };

  componentDidMount() {
    const config = {
      headers: {
        Authorization: `Bearer ${getCustomer()}`,
        "Content-Type": "application/json",
      },
    };

    getAllChatRooms(config, this.getroomSuccessCallback);
  }

  getroomSuccessCallback = (response) => {
    console.log(response, "incommingresponse");
    // window.location = "/ChatWithCarrier";
    this.setState({ allchatrooms: response.data.allrooms });
    console.log(this.state.allchatrooms, "chatrooms");
  };

  render() {
    return (
      <div
        className="shadow-sm"
        style={{
          background: "#ffffff",
          height: "82vh",
          borderRadius: "10px",
          overflowX: "none",
          overflowY: "auto",
          msOverflowStyle: "none",
        }}
      >
        <h2 className="text-center my-4">Shipments</h2>
        {this.state.allchatrooms.map((ship) => {
          console.log(ship, "roominfo");
          return (
            <div
              onClick={() => this.props.selectroomcallback(ship.roomname)}
              key={ship._id}
              className="container chatshipment"
            >
              <div className="d-flex flex-column ml-3">
                <div className="">{ship.roomname}fe</div>
              </div>
            </div>
          );
        })}
      </div>
    );
  }
}

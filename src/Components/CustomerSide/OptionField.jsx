import React from "react";
const OptionField = ({
  labelTitle,
  name,
  error,
  Option,
  value,
  changeHandler,
}) => {
  return (
    <React.Fragment>
      <label
        className="font-weight-bolder"
        style={{ color: "#B0BAC9", fontSize: "15px" }}
      >
        {labelTitle}
      </label>
      <select
        name={name}
        id={name}
        value={value}
        onChange={changeHandler}
        className="form-control"
      >
        <option value="Select"></option>
        {Option.map((item, i) => {
          return (
            <option key={i} value={item}>
              {item}
            </option>
          );
        })}
      </select>
      {error && <div className="alert alert-danger">{error}</div>}
    </React.Fragment>
  );
};
export default OptionField;

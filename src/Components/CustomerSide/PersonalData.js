import React, { Component } from "react";
import Option from "../Common/Option";
import userpic from "../../Pics/userpic.png";
import Switch from "react-switch";
import "../../Styles/CustomerHome.css";
import { UpdateCustomerObj } from "../../Actions/actioncreators";
import { connect } from "react-redux";
import { getCustomer, getCustomerUserId } from "../../LocalStorage/Auth";
const Options = [
  { id: 1, value: "English" },
  { id: 2, value: "Irish" },
];
class PersonalData extends Component {
  state = {
    data: { ...this.props.customer },
    language: "",
    customer_address: "",
    customer_phonenum: "",
    customer_username: "",
    loadingButton: false,
    smsNotification: false,
    emailNotification: false,
  };
  changeHandler = ({ target }) => {
    const data = { ...this.state.data };
    data[target.name] = target.value;
    this.setState({ data });
  };
  //---------FOR SWITCH TOGGLER----
  handleSwitchEmail = () => {
    this.setState({ emailNotification: !this.state.emailNotification });
  };
  handleSwitchSms = () => {
    this.setState({ smsNotification: !this.state.smsNotification });
  };
  loadingButton = () => {
    this.setState({ loadingButton: true });
  };
  //----handle the form
  formPersonalData = (e) => {
    e.preventDefault();
    this.loadingButton();
    const {
      data,
      customer_username,
      customer_address,
      smsNotification,
      emailNotification,
      language,
      customer_phonenum,
    } = this.state;

    const token = getCustomer();
    const config = {
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
      },
    };
    const requestBody = {
      customer_name: data.customer_name,
      customer_username: customer_username,
      customer_email: data.customer_email,
      customer_mobilenum: data.customer_mobilenum,
      customer_address: customer_address,
      customer_smsNotify: smsNotification,
      customer_emailNotify: emailNotification,
      customer_language: language,
      customer_phonenum: customer_phonenum,
    };
    const { id: customerId } = getCustomerUserId();
    this.props.updateCustomer(
      requestBody,
      config,
      this.updateSuccess,
      this.updateFail,
      customerId
    );
  };
  updateSuccess = () => {
    this.setState({ loadingButton: false });
    alert("Update successfully");
  };
  updateFail = () => {
    this.setState({ loadingButton: false });
    alert("Update Not done");
  };
  //-----render input field data coming from DB-----------------
  renderInput(labelTilte, fieldname, fieldType = "text") {
    const { data } = this.state;
    return (
      <React.Fragment>
        <label className="text-muted">{labelTilte}</label>
        <input
          type={fieldType}
          name={fieldname}
          className="form-control"
          onChange={this.changeHandler}
          value={data[fieldname]}
          required
        />
      </React.Fragment>
    );
  }
  //--------------newly data enter-----------------------
  renderNewInput(labelTilte, fieldname, fieldType = "text") {
    return (
      <React.Fragment>
        <label className="text-muted">{labelTilte}</label>
        <input
          type={fieldType}
          name={fieldname}
          className="form-control"
          onChange={this.changeHandler}
          value={this.state.fieldname}
        />
      </React.Fragment>
    );
  }
  render() {
    const { loadingButton } = this.state;
    return (
      <div className="shadow-sm" style={{ background: "#ffffff" }}>
        <br />
        <span className="text-muted pl-3">Persona Data</span>
        <div className="main__div">
          <img
            src={userpic}
            alt="userpic"
            className="img__position"
            style={{ width: "140px", height: "140px" }}
          />
        </div>
        <span className="font-weight-bolder pl-3">
          Enter your Personal Information
        </span>
        <div className="form p-3">
          <form onSubmit={this.formPersonalData}>
            <div className="form-row">
              <div className="form-group col-md-6">
                {this.renderInput("Name", "customer_name")}
              </div>
              <div className="form-group col-md-3 col-sm-6">
                {this.renderInput("Email", "customer_email", "email")}
              </div>
              <div className="form-group col-md-3 col-sm-6">
                {this.renderNewInput("User Name", "customer_username")}
              </div>
            </div>
            <div className="form-row">
              <div className="form-group col-md-3">
                {this.renderInput("Mobile Number", "customer_mobilenum", "tel")}
              </div>
              <div className="form-group col-md-3">
                {this.renderNewInput(
                  "Phone Number",
                  "customer_phonenum",
                  "tel"
                )}
              </div>
              <div className="form-group col-md-4">
                {this.renderNewInput("Address", "customer_address")}
              </div>
              <div className="form-group col-md-2">
                <Option
                  labelTitle="Language"
                  name="language"
                  Options={Options}
                  Value={this.state.language}
                  changeHandler={this.changeHandler}
                />
              </div>
            </div>
            <div className="form-row mt-3">
              <div className="form-group col-md-3">
                <label className="newShipmentLabel">Email Notification</label>
                <br />
                <Switch
                  onChange={this.handleSwitchEmail}
                  checked={this.state.emailNotification}
                />
              </div>
              <div className="form-group col-md-3">
                <label className="newShipmentLabel">SMS Notification</label>
                <br />
                <Switch
                  onChange={this.handleSwitchSms}
                  checked={this.state.smsNotification}
                />
              </div>
            </div>
            <div className="form-row mt-3">
              <div className="form-group col-md-3">
                <button
                  className="btn btn-primary btn-block"
                  type="submit"
                  disabled={loadingButton}
                >
                  {loadingButton ? <span>Loading ...</span> : <span>Save</span>}
                </button>
              </div>
            </div>
          </form>
        </div>
      </div>
    );
  }
}
const mapDispatchToProps = (dispatch) => {
  return {
    updateCustomer: (
      requestBody,
      config,
      updateSuccess,
      updateFail,
      customerId
    ) => {
      dispatch(
        UpdateCustomerObj(
          requestBody,
          config,
          updateSuccess,
          updateFail,
          customerId
        )
      );
    },
  };
};
export default connect(null, mapDispatchToProps)(PersonalData);

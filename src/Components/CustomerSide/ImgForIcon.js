import React, { Component } from "react";
import "../../Styles/NewShipment.css";

export default class ImgForIcon extends Component {
  render() {
    const { shipment_type } = this.props.state;
    const { title, pic, type, onClickShipment } = this.props;
    return (
      <React.Fragment>
        <div
          className={
            shipment_type === type
              ? "col-md-3 m-2 ml-5 rounded border_default active_shipment"
              : "col-md-3 m-2 ml-5 rounded border_default"
          }
          onClick={() => {
            onClickShipment(type, title);
          }}
        >
          <div className="d-flex">
            <img
              src={pic}
              className="mr-2"
              alt="shipicon"
              style={{ width: "70px", height: "70px" }}
            />
            <div className="d-flex flex-column align-self-center">
              <span style={{ fontWeight: 600 }}>{type}</span>
              <small className="text-muted">{title}</small>
            </div>
            {shipment_type === type && (
              <div className="tick_mark">&#10004;</div>
            )}
          </div>
        </div>
      </React.Fragment>
    );
  }
}

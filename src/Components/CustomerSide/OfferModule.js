import React from "react";
import OfferApiCall from "./OfferApiCall";
import { connect } from "react-redux";
import { withRouter } from "react-router-dom";
import {
  AcceptSplitCustomerOffer,
  AcceptSimpleCustomerOffer,
} from "../../Actions/actioncreators";

class OfferModule extends OfferApiCall {
  render() {
    console.log(this.props.offer)
    const {
      date_pickup,
      date_dropoff,
      cost,
      _id: offerID,
      carrier,
    } = this.props.offer;
    const datepickup = date_pickup.slice(0, 10);
    const datedropoff = date_dropoff.slice(0, 10);
    const selectedshipment = this.props.CustomerShipments.find(
      (el) => el._id === this.props.match.params.id
    );
    console.log("selected shipment", selectedshipment);
    if (selectedshipment === undefined) {
      this.props.history.push("/Shipments");
      return null;
    } else
      return (
        <React.Fragment>
          <hr />
          <div className="row">
            <div className="col-3">
              <span className="fontbold fontsize d-block">Pickup Date</span>
              <span className="text-muted">{datepickup}</span>
              <span className="fontbold fontsize d-block">Delivery Date</span>
              <span className="text-muted">{datedropoff}</span>
            </div>
            <div className="col-3">
              <span className="fontbold fontsize d-block">Pickup Time</span>
              <span className="text-muted">---</span>
              <span className="fontbold fontsize d-block">Delivery Time</span>
              <span className="text-muted">---</span>
            </div>
            <div className="col-3">
              <span className="fontbold fontsize d-block">Types of Truck</span>
              <span className="text-muted">---</span>
            </div>
            <div className="col-3">
              <span className="fontbold fontsize  mx-auto ">Price</span>
              <span className="text-muted d-block mx-auto ">{cost}$</span>

              {selectedshipment.offer_accepted &&
              selectedshipment.offer_accepted === this.props.offer._id ? (
                <button className="btn mybtn-green mx-auto disabled mt-3">
                  Accepted
                </button>
              ) : (
                <button
                  className="mybtn-green mx-auto mt-3"
                  onClick={() => {
                    this.offerClicked(
                      offerID,
                      selectedshipment,
                      carrier._id
                    );
                  }}
                >
                  Accept
                </button>
              )}
            </div>
          </div>
          <hr />
        </React.Fragment>
      );
  }
}

const mapStateToProps = (state) => {
  return {
    CustomerShipments: state.customerShipmentReducer.CustomerShipments,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    AcceptSpliteOffer: (
      config,
      shipment,
      OfferAcceptSuccess,
      OfferAcceptFailure
    ) =>
      dispatch(
        AcceptSplitCustomerOffer(
          config,
          shipment,
          OfferAcceptSuccess,
          OfferAcceptFailure
        )
      ),
    AcceptSimpleOffer: (
      requestbody,
      config,
      shipment,
      OfferAcceptSuccess,
      OfferAcceptFailure
    ) =>
      dispatch(
        AcceptSimpleCustomerOffer(
          requestbody,
          config,
          shipment,
          OfferAcceptSuccess,
          OfferAcceptFailure
        )
      ),
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withRouter(OfferModule));

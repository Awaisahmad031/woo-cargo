import React, { Component } from "react";
import { connect } from "react-redux";
import { ChangeCustomerPassword } from "../../Actions/actioncreators";
import {
  getCustomer,
  getCustomerUserId,
  removeToken,
} from "../../LocalStorage/Auth";

class ChangePassword extends Component {
  state = {
    data: { oldpassword: "", newpassword: "", renewpassword: "" },
    newPassError: false,
    oldPassError: false,
    loadingButton: false,
  };
  //After form submit
  passwordForm = (e) => {
    e.preventDefault();
    const { data } = this.state;
    this.loadingButton();
    const isSame = this.verifyPassword();
    if (!isSame)
      return this.setState({ newPassError: true, loadingButton: false });
    const token = getCustomer();
    const { id: customerId } = getCustomerUserId();
    const config = {
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
      },
    };
    const requestbody = {
      oldpass: data.oldpassword,
      newpass: data.newpassword,
    };
    this.props.changePassword(
      requestbody,
      config,
      customerId,
      this.successChange,
      this.failSuccess
    );
  };
  loadingButton = () => {
    this.setState({ loadingButton: true });
  };
  successChange = () => {
    alert("Successfully Change the Password.Now you have to Login");
    removeToken();
  };
  failSuccess = () => {
    this.setState({ oldPassError: true });
  };
  changeHandler = ({ target }) => {
    this.setState({ newPassError: false, oldPassError: false });
    const data = { ...this.state.data };
    data[target.name] = target.value;
    this.setState({ data });
  };

  //Verifying the password enter by user
  verifyPassword = () => {
    const { newpassword, renewpassword } = this.state.data;
    if (newpassword !== renewpassword) return false;
    return true;
  };
  //Render Input field
  renderInput(labelTitle, fieldname, typename = "password") {
    const { data } = this.state;
    return (
      <React.Fragment>
        <label className="text-muted">{labelTitle}</label>
        <input
          type={typename}
          name={fieldname}
          className="form-control"
          onChange={this.changeHandler}
          value={data.fieldname}
          required
        />
      </React.Fragment>
    );
  }
  render() {
    const { newPassError, oldPassError } = this.state;
    return (
      <div
        className="container shadow-sm my-5 pb-3"
        style={{ background: "#ffffff" }}
      >
        <br />
        <span className="text-muted">Password</span>
        {newPassError && (
          <div
            className="alert alert-danger float-right"
            style={{ fontSize: "14px" }}
          >
            New and Re-New Password are not same
          </div>
        )}
        {oldPassError && (
          <div
            className="alert alert-danger float-right"
            style={{ fontSize: "14px" }}
          >
            Old Password is not Correct
          </div>
        )}
        <br />
        <p className="font-weight-bolder mt-3">&nbsp;Change Your Password</p>
        <form onSubmit={this.passwordForm} className="mt-5">
          <div className="form-row">
            <div className="form-group col-md-4">
              {this.renderInput("Old Password", "oldpassword")}
            </div>
            <div className="form-group col-md-4">
              {this.renderInput("New Password", "newpassword")}
            </div>
            <div className="form-group col-md-4">
              {this.renderInput("Re-New Password", "renewpassword")}
            </div>
          </div>
          <div className="form-row mt-2">
            <div className="form-group col-md-3">
              <button
                className="btn btn-primary btn-block"
                type="submit"
                disabled={this.state.loadingButton}
              >
                {this.state.loadingButton ? (
                  <span>Loading...</span>
                ) : (
                  <span> Change Password</span>
                )}
              </button>
            </div>
          </div>
        </form>
      </div>
    );
  }
}
const mapDispatchToprops = (dispatch) => {
  return {
    changePassword: (
      requestbody,
      config,
      customerId,
      successChange,
      failSuccess
    ) => {
      dispatch(
        ChangeCustomerPassword(
          requestbody,
          config,
          customerId,
          successChange,
          failSuccess
        )
      );
    },
  };
};
export default connect(null, mapDispatchToprops)(ChangePassword);

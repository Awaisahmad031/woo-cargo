import React from "react";
import "../../Styles/CustomerHome.css";

export default function EmptyOffer() {
  return (
    <div
      className="container p-4 shadow-sm"
      style={{
        backgroundColor: "#ffffff",
        position: "relative",
        height: "35vh",
        overflowX: "none",
        overflowY: "auto",
      }}
    >
      <div className="row">
        <span className="ml-2 fontsize" style={{ color: "#0000FF" }}>
          Offers Information
        </span>
      </div>
      <h2 className="PositionEmptyOffer">No Offers Found</h2>
    </div>
  );
}

import React, { Component } from "react";
import Images from "../../Pics/Images";
import ImgForIcon from "./ImgForIcon";

let shipmeticon = [
  { id: 1, type: "LTL", title: "First Mile,Last Mile", pic: Images.shipicon1 },
  { id: 2, type: "FTL", title: "Full Trucks", pic: Images.shipicon2 },
  {
    id: 3,
    type: "DRYAGE",
    title: "The Future of Drayage",
    pic: Images.shipicon3,
  },
  {
    id: 4,
    type: "REMOVAL",
    title: "Home and Business",
    pic: Images.shipicon4,
  },
  {
    id: 5,
    type: "CARS & BOATS",
    title: "Low costs Cars",
    pic: Images.shipicon5,
  },
  {
    id: 6,
    type: "HEAVY MACHINARY",
    title: "Low costs shipming",
    pic: Images.shipicon6,
  },
];

export default class TypesOfShipments extends Component {
  state = {
    shipment_type: "",
    shipment_title: "",
  };
  onClickShipment = (type, title) => {
    this.setState({ shipment_type: type, shipment_title: title });
  };
  componentDidUpdate(prevProps) {
    if (
      prevProps.state.shipment_type !== this.state.shipment_type &&
      prevProps.state.shipment_title !== this.state.shipment_title
    )
      this.props.shipmentType(this.state);
  }

  render() {
    return (
      <React.Fragment>
        <div
          className="container mt-5 shadow-sm p-4"
          style={{ background: "#ffffff" }}
        >
          <p className="pt-4" style={{ color: "#8798AD" }}>
            TYPE OF SHIPMENT
          </p>
          <p>Select the types of shipment</p>
          <div className="row mt-3">
            {shipmeticon.map((item) => {
              return (
                <ImgForIcon
                  key={item.id}
                  type={item.type}
                  title={item.title}
                  pic={item.pic}
                  state={this.state}
                  onClickShipment={this.onClickShipment}
                />
              );
            })}
          </div>
        </div>
      </React.Fragment>
    );
  }
}

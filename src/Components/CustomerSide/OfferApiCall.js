import { Component } from "react";
import { getCustomer } from "../../LocalStorage/Auth";

export default class OfferApiCall extends Component {
  offerClicked = (offerID, selectedshipment, carrierid) => {
    console.log("offer clicked", selectedshipment, offerID);
    const token = getCustomer();
    const carrierID = carrierid;
    const { _id: shipmentID, split_route } = selectedshipment;
    split_route
      ? this.splitOfferApi(token, shipmentID)
      : this.simpleOfferApi(token, carrierID, offerID, shipmentID);
  };
  //-----------SIMPLE OFFER------------
  simpleOfferApi = (token, carrierID, offerID, shipmentID) => {
    console.log("carrierID", carrierID);
    const config = {
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
      },
    };
    const requestbody = {
      carrier: carrierID,
      _id: offerID,
    };
    const shipment = {
      id: shipmentID,
    };
    this.props.AcceptSimpleOffer(
      requestbody,
      config,
      shipment,
      this.OfferAcceptSuccess,
      this.OfferAcceptFailure
    );
  };
  //-------------SPLIT OFFER-----------------
  splitOfferApi = (token, shipmentID) => {
    const config = {
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
      },
    };
    const shipment = {
      id: shipmentID,
    };
    this.props.AcceptSpliteOffer(
      config,
      shipment,
      this.OfferAcceptSuccess,
      this.OfferAcceptFailure
    );
  };
  OfferAcceptSuccess = () => {
    console.log("Offer accept success");
    alert("offer accepted");
  };
  OfferAcceptFailure = () => {
    console.log("Offer accept fail");
    alert("fail to accept offer");
  };
}

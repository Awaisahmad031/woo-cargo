import React from "react";
import OfferModule from "./OfferModule";
import EmptyOffer from "./EmptyOffer";
import { getCustomer } from "../../LocalStorage/Auth";
import { withRouter } from "react-router-dom";
import addToChatRoom from "../../Services/addtochatroom";
import "../../Styles/DetailCusShip.css";

const OfferInformation = (props) => {
  if (props.offers.offers) {
    console.log(props.offers.offers, "incomming offers Length");

    if (props.offers.offers.length === 0) return <EmptyOffer />;
  }

  const onChatPressed = () => {
    //call service to add room to this shipment

    const config = {
      headers: {
        Authorization: `Bearer ${getCustomer()}`,
        "Content-Type": "application/json",
      },
    };

    const reqbody = {
      roomname: props.shipmentid,
      shipment: props.shipmentid,
      participant: "DRIVER",
    };
    addToChatRoom(reqbody, config, addroomSuccessCallback);
  };

  const addroomSuccessCallback = (response) => {
    console.log(response, "incommingresponse");
    window.location = "/ChatWithCarrier";
  };

  return (
    <div
      className="container p-4 shadow-sm"
      style={{
        backgroundColor: "#ffffff",
        height: "35vh",
        overflowX: "none",
        overflowY: "auto",
        msOverflowStyle: "none",
      }}
    >
      <div className="row">
        <span className="ml-2 fontsize" style={{ color: "#0000FF" }}>
          Offer Informations
        </span>
        <button onClick={onChatPressed} className="mybtn-blue  ml-auto mr-5 ">
          <span>Chat</span>
        </button>
      </div>
      {props.offers.offers
        ? props.offers.offers.map((item, index) => {
            return <OfferModule key={index} offer={item} />;
          })
        : ""}
    </div>
  );
};

export default withRouter(OfferInformation);

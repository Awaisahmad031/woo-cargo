import React, { Component } from "react";
import MessageBox from "../Common/MessageBox";
import chatSocket from "../../socket/driverchatsocket";
import { getCustomerUserId, getCustomer } from "../../LocalStorage/Auth";
import getChatHistory from "../../Services/getchathistory";

export default class ChatArea extends Component {
  state = {
    message: "",
    all_messages: [],
    roomselected: false,
  };

  // msgobj = {
  //   msg_content:String,
  //   shipment_id:String,
  //   from_user:String(userid),
  //   msg_date_time:Date
  // }

  componentDidMount() {
    //Tell User to select a room
  }

  componentDidUpdate(prevProps) {
    if (prevProps.roomselected !== this.props.roomselected) {
      console.log("joining room");
      this.joinnewchatroom();
    }
    console.log(this.props.roomselected, "Current Selectd Room");
  }

  joinnewchatroom() {
    console.log(this.props.roomselected, "RoomSelected");
    chatSocket.emit("joinchatroom", this.props.roomselected, (room) => {
      console.log(`${room} room joined`, "RoomSelected");
      this.setState({ roomselected: true, all_messages: [], message: "" });

      getChatHistory(
        room,
        {
          headers: {
            Authorization: `Bearer ${getCustomer()}`,
            "Content-Type": "application/json",
          },
        },
        this.onchathistorySuccessCallback
      );

      chatSocket.on("chatmsg", (msg) => {
        this.msgrecieved(msg);
      });

      chatSocket.on("disconnect", function () {
        console.log("disconnected to socket");
      });
    });
  }

  onchathistorySuccessCallback = (allmessages) => {
    console.log(allmessages);
    this.setState({ all_messages: allmessages });
  };

  changeHandler = ({ target }) => {
    this.setState({ message: target.value });
  };
  messageSend = (e) => {
    e.preventDefault();
    const all_messages = [...this.state.all_messages];
    const msg = {
      msg_content: this.state.message,
      shipment_id: this.props.roomselected,
      from_user: getCustomerUserId().id,
    };
    all_messages.push(msg);
    this.setState({ all_messages, message: "" });

    chatSocket.emit("sendchatmessage", msg);
  };
  msgrecieved = (msg) => {
    const allmessages = [...this.state.all_messages];
    allmessages.push(msg);
    this.setState({ all_messages: allmessages });
  };

  render() {
    return (
      <React.Fragment>
        {/*----------------CHAT AREA-------------------- */}
        <div
          className="container"
          style={{
            height: "70vh",
            overflowY: "auto",
            borderRadius: "10px",
            overflowX: "none",
            msOverflowStyle: "none",
          }}
        >
          {/*----------------MSG BOX----------- */}
          {this.state.roomselected ? (
            <MessageBox message={this.state.all_messages} />
          ) : (
            <p>Select a Shipment from sidebar to start Chat</p>
          )}
        </div>
        {/*--------------ENTER MESSAGE-------------------- */}
        <div
          className="p-3 ml-1 shadow-sm"
          style={{ background: "#ffffff", borderRadius: "10px" }}
        >
          <form onSubmit={this.messageSend}>
            <div className="form-row">
              <div className="col-10">
                <input
                  type="text"
                  name="message"
                  className="form-control"
                  autoComplete="off"
                  value={this.state.message}
                  onChange={this.changeHandler}
                  placeholder="Type a message...."
                />
              </div>
              <div className="col-2">
                <button type="submit" className="btn btn-block btn-primary">
                  Send
                </button>
              </div>
            </div>
          </form>
        </div>
      </React.Fragment>
    );
  }
}

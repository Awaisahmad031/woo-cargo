import React, { Component } from "react";
import "../../Styles/DetailCusShip.css";
import {transform_shipment_attributes} from '../../globals'
export default class DetailInfoCard extends Component {
  render() {
    const { shipment } = this.props;
    const {
      _id,
      shipment_type,
      shipment_description: description,
      shipment_attributes,
    } = shipment;
    return (
      <div className="container p-4 shadow-sm"
        style={{
          backgroundColor: "#ffffff",
          height: "35vh",
          overflowX: "none",
          overflowY: "auto",
          msOverflowStyle: "none",
        }}
      >
        <div className="row">
          <span className="ml-2  fontsize" style={{ color: "#0000FF" }}>
            Shipping Details
          </span>
          <button className="mybtn-blue-secondary ml-auto mr-5 fontsize">
            Edit
          </button>
        </div>
        <hr />

        <div className="body">
          <span className="fontbold fontsize mr-1">ID #</span>
          <span className="text-muted fontsize">{_id}</span>
          <span className="ml-5 fontbold fontsize mr-1">Type:</span>
          <span className="text-muted fontsize">{shipment_type}</span>
          <span className="fontbold fontsize d-block">Description</span>
          <span className="text-muted fontsize">{description}</span>
          <span className="fontbold fontsize d-block">Notes</span>
          <span className="text-muted fontsize">
            {shipment.notes ? shipment.notes : ""}
          </span>
          <hr />
        </div>
        {/*---IF SHIPMENT TYPE REMOVALS THEN SHOW REACT SWITCH INFO---- */}
        <div className='col-12 row'>
        {Object.keys(transform_shipment_attributes(shipment_attributes)).map(attribute=>{
                return(
                  <div className='d-flex flex-column col-3' >
                  <span className="black_text">{attribute}</span>
                  <span className="gray_text">{transform_shipment_attributes(shipment_attributes)[attribute]}</span>
                  </div>
                )
            })}
      </div>
   </div>
    );
  }
}

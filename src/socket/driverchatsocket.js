import socketIOClient from "socket.io-client";
import {
  getCustomerUserId,
  getCustomer,
  getCustomerObj,
} from "../LocalStorage/Auth";

let chatSocket; // driverchat

chatSocket = socketIOClient(`http://127.0.0.1:8000/driverchat`);

chatSocket.on("connect", () => {
  console.log("connected to chatSocket ");
});

export default chatSocket;

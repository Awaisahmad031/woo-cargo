import socketIOClient from "socket.io-client";
import { notify } from "../Components/Common/Notification";
import {
  getCustomerUserId,
  getCustomer,
  getCustomerObj,
} from "../LocalStorage/Auth";

let notificationsocket;

if (getCustomerUserId()) {
  notificationsocket = socketIOClient(
    `http://127.0.0.1:8000/notifications?userid=${getCustomerUserId().id}`
  );
  notificationsocket.on("connect", () => {
    console.log("connected to notifications socket");
  });

  notificationsocket.on("notification", (notificationobj) => {
    notify(notificationobj.notification_content);
  });
}

export default notificationsocket;

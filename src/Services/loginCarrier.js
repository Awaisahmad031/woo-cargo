import axios from "axios";
import { URL } from "../globals";
import { LoginActionCarrier } from "../Actions/actions";

const loginServiceCarrier = (
  requestbody,
  config,
  dispatch,
  LoginSucessCarrier,
  LoginFailureCarrier
) => {
  axios
    .post(`${URL}api/carriers/login`, requestbody, config)
    .then(function (response) {
      dispatch(LoginActionCarrier(response.data));

      //callback function to change routes or etc
      LoginSucessCarrier(response.data.authToken, response.data.carrier);
    })
    .catch(function (error) {
      LoginFailureCarrier();
      if (error.response) {
        // The request was made and the server responded with a status code
        // that falls out of the range of 2xx
        console.log(error.response.data);
        console.log(error.response.status);
        console.log(error.response.headers);
        console.log("Error", error.message);

        console.log("error 1");
      } else if (error.request) {
        // The request was made but no response was received
        // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
        // http.ClientRequest in node.js
        console.log(error.request);
      } else {
        // Something happened in setting up the request that triggered an Error
        console.log("Error", error.message);
      }
      console.log(error.config);
    });
};

export default loginServiceCarrier;

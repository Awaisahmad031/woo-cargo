import axios from "axios";
import { URL } from "../globals";
import { AddDriverTruckToShipmentAction } from "../Actions/actions";

const addTruckDriverToSplit = (
  requestbody,
  config,
  dispatch,
  shipment,
  AddSucess,
  AddFailure
) => {
  axios
    .post(
      `${URL}api/shipments/${shipment.id}/addtruckdriver/`,
      requestbody,
      config
    )
    .then(function (response) {
      dispatch(AddDriverTruckToShipmentAction(response.data));
      //callback function to change routes or etc
      AddSucess();
    })
    .catch(function (error) {
      AddFailure();
      if (error.response) {
        // The request was made and the server responded with a status code
        // that falls out of the range of 2xx
        console.log(error.response.data);
        console.log(error.response.status);
        console.log(error.response.headers);
        console.log("Error", error.message);

        console.log("error 1");
      } else if (error.request) {
        // The request was made but no response was received
        // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
        // http.ClientRequest in node.js
        console.log(error.request);
      } else {
        // Something happened in setting up the request that triggered an Error
        console.log("Error", error.message);
      }
      console.log(error.config);
    });
};

export default addTruckDriverToSplit;

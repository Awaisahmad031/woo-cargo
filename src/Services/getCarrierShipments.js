import axios from "axios";
import { URL } from "../globals";
import { GetCarrierShipmentsAction } from "../Actions/actions";

const getCarrierShipments = (
  requestbody,
  config,
  dispatch,
  GetShipmentSucess,
  GetShipmentFailure
) => {
  axios
    .get(`${URL}api/carriers/${requestbody.carrierid}/newshipments`, config)
    .then(function(response) {
      console.log("service", response.data);
      dispatch(GetCarrierShipmentsAction(response.data));
      //callback function to change routes or etc
      GetShipmentSucess();
    })
    .catch(function(error) {
      GetShipmentFailure();
      if (error.response) {
        // The request was made and the server responded with a status code
        // that falls out of the range of 2xx
        console.log(error.response.data);
        console.log(error.response.status);
        console.log(error.response.headers);
        console.log("Error", error.message);

        console.log("error 1");
      } else if (error.request) {
        // The request was made but no response was received
        // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
        // http.ClientRequest in node.js
        console.log(error.request);
      } else {
        // Something happened in setting up the request that triggered an Error
        console.log("Error", error.message);
      }
      console.log(error.config);
    });
};

export default getCarrierShipments;

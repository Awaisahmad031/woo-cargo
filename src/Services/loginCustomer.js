import axios from "axios";
import { URL } from "../globals";
import { LoginAction } from "../Actions/actions";

const loginService = (
  requestbody,
  config,
  dispatch,
  LoginSucessCustomer,
  LoginFailureCustomer
) => {
  axios
    .post(`${URL}api/customers/login`, requestbody, config)
    .then(function (response) {
      dispatch(LoginAction(response.data));
      //callback function to change routes or etc
      LoginSucessCustomer(response.data.authToken, response.data.customer);
    })
    .catch(function (error) {
      LoginFailureCustomer();
      console.log(error);
    });
};

export default loginService;

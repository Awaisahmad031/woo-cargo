import axios from "axios";
import { URL } from "../globals";

const updateCustomer = (
  requestbody,
  config,
  dispatch,
  customerId,
  successChange,
  failSuccess
) => {
  axios
    .put(`${URL}api/customers/${customerId}/resetpass`, requestbody, config)
    .then(function (response) {
      console.log(response.data);
      successChange();
    })
    .catch(function (error) {
      failSuccess();
      console.log(error);
    });
};

export default updateCustomer;

import axios from "axios";
import { URL } from "../globals";
import { resetWarningCache } from "prop-types";
// import { updateCustomerAction } from "../Actions/actions";

const updateCustomer = (
  requestBody,
  config,
  dispatch,
  updateSuccess,
  updateFail,
  customerId
) => {
  console.log("service", requestBody);
  axios
    .put(`${URL}api/customers/${customerId}/updateprofile`, requestBody, config)
    .then(function (response) {
      console.log(response.data);
      //   dispatch(updateCustomerAction(response.data));
      //callback function to change routes or etc
      updateSuccess();
    })
    .catch(function (error) {
      updateFail();
      console.log(error);
    });
};

export default updateCustomer;

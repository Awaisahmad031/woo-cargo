import axios from "axios";
import { URL } from "../globals";
import { SignupActionCustomer } from "../Actions/actions";

const signupServiceCustomer = (
  requestbody,
  config,
  dispatch,
  SignupSucessCustomer,
  signupFailure
) => {
  console.log(requestbody, config);
  axios
    .post(`${URL}api/customers`, requestbody, config)
    .then(function (response) {
      dispatch(SignupActionCustomer(response.data));
      //callback function to change routes or etc
      SignupSucessCustomer(response.data.authToken, response.data.customer);
    })
    .catch(function (error) {
      signupFailure();
      if (error.response) {
        // The request was made and the server responded with a status code
        // that falls out of the range of 2xx
        console.log(error.response.data);
        console.log(error.response.status);
        console.log(error.response.headers);
        console.log("Error", error.message);

        console.log("error 1");
      }
    });
};

export default signupServiceCustomer;

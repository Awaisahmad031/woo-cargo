import loginCustomer from "../Services/loginCustomer";
import updateCustomer from "../Services/updateCustomer";
import changeCustomerPassword from "../Services/ChangePassword";
import loginCarrier from "../Services/loginCarrier";
import signupCustomer from "../Services/signupCustomer";
import AddShipmentCustomer from "../Services/AddShipmentCustomer";
import GetShipmentCustomer from "../Services/GetShipmentCustomer";
import GetDrivers from "../Services/GetDrivers";
import GetVehicals from "../Services/GetVehicals";
import AddVehical from "../Services/addVehical";
import AddDriver from "../Services/addDriver";
import getCarrierShipmnets from "../Services/getCarrierShipments";
import getCustomerOffer from "../Services/getCustomerOffer";
import SimpleCustomerOffer from "../Services/SimpleCustomerOffer";
import SplitCustomerOffer from "../Services/SplitCustomerOffer";
import AddTruckDriver from "../Services/AddTruckDriver";
import AddTruckDriverSplit from "../Services/AddTruckDriverToSplit";
import CarrierOfferSendService from "../Services/CarrierOfferSend";
import getShipmentbyId from "../Services/getshipmentbyid";
import getShipmentbyIdCarrier from "../Services/getshipmentbyidCarrier";
import CustomerNotification from "../Services/CustomerNotification";
import CarrierNotification from "../Services/CarrierNotification";
import EditDriverService from "../Services/EditDriverService";
import DeleteDriverService from "../Services/DeleteDriverService";
import EditVehicalService from "../Services/EditVehicalService";
import DeleteVehicalService from "../Services/DeleteVehicalService";
//-------------------------CUSTOMER LOGIN---------------
export const CustomerLogin = (
  requestbody,
  config,
  LoginSucessCustomer,
  LoginFailureCustomer
) => {
  return (dispatch) => {
    //axios service call here and pass dispatch args
    loginCustomer(
      requestbody,
      config,
      dispatch,
      LoginSucessCustomer,
      LoginFailureCustomer
    );
  };
};

//-------------------------CUSTOMER UPDATE---------------
export const UpdateCustomerObj = (
  requestBody,
  config,
  updateSuccess,
  updateFail,
  customerId
) => {
  return (dispatch) => {
    //axios service call here and pass dispatch args
    updateCustomer(
      requestBody,
      config,
      dispatch,
      updateSuccess,
      updateFail,
      customerId
    );
  };
};
//----------------------CHANGE CUSTOMER PASSWORD-------------

export const ChangeCustomerPassword = (
  requestbody,
  config,
  customerId,
  successChange,
  failSuccess
) => {
  return (dispatch) => {
    //axios service call here and pass dispatch args
    changeCustomerPassword(
      requestbody,
      config,
      dispatch,
      customerId,
      successChange,
      failSuccess
    );
  };
};
//-------------------------CARRIER LOGIN----------------------
export const CarrierLogin = (
  requestbody,
  config,
  LoginSucessCarrier,
  LoginFailureCarrier
) => {
  return (dispatch) => {
    loginCarrier(
      requestbody,
      config,
      dispatch,
      LoginSucessCarrier,
      LoginFailureCarrier
    );
  };
};
//--------------------CUSTOMER SIGNUP--------------
export const CustomerSignup = (
  requestbody,
  config,
  signupSucessCustomer,
  signupFailure
) => {
  return (dispatch) => {
    signupCustomer(
      requestbody,
      config,
      dispatch,
      signupSucessCustomer,
      signupFailure
    );
  };
};
//--------------------------ADD CUSTOMER SHIPMENT---------------

export const AddShipmentCustomerSide = (
  requestbody,
  config,
  AddShipmentSucessCustomer,
  AddShipmentFailureCustomer
) => {
  return (dispatch) => {
    AddShipmentCustomer(
      requestbody,
      config,
      dispatch,
      AddShipmentSucessCustomer,
      AddShipmentFailureCustomer
    );
  };
};
//-------------------------GET ALL CUSTOMER SHIPMENTS---------
export const GetShipmentCustomerSide = (
  requestbody,
  config,
  GetShipmentSucess,
  GetShipmentFailure
) => {
  return (dispatch) => {
    GetShipmentCustomer(
      requestbody,
      config,
      dispatch,
      GetShipmentSucess,
      GetShipmentFailure
    );
  };
};
//----------------------------GET ALL DRIVERS CARRIER SIDE----------------
export const GetDriversCarrierSide = (
  requestbody,
  config,
  GetDriversSucess,
  GetDriversFailure
) => {
  return (dispatch) => {
    GetDrivers(
      requestbody,
      config,
      dispatch,
      GetDriversSucess,
      GetDriversFailure
    );
  };
};
//-----------------------------GET ALL VEHICALS CARRIER SIDE--------
export const GetVehicalsCarrierSide = (
  requestbody,
  config,
  GetVehicalsSucess,
  GetVehicalsFailure
) => {
  return (dispatch) => {
    GetVehicals(
      requestbody,
      config,
      dispatch,
      GetVehicalsSucess,
      GetVehicalsFailure
    );
  };
};
//------------------------------ADD DRIVER----------------------
export const AddDriverCarrierSide = (
  requestbody,
  config,
  AddDriverSucess,
  AddDriverFailure
) => {
  return (dispatch) => {
    AddDriver(requestbody, config, dispatch, AddDriverSucess, AddDriverFailure);
  };
};
//----------------------------ADD VEHICAL----------------
export const AddVehicalCarrierSide = (
  requestbody,
  config,
  AddVehicalSucess,
  AddVehicalFailure
) => {
  return (dispatch) => {
    AddVehical(
      requestbody,
      config,
      dispatch,
      AddVehicalSucess,
      AddVehicalFailure
    );
  };
};
//--------------------------GET CARRIERS SHIPMENTS------------------------

export const GetShipmentsCarrierSide = (
  requestbody,
  config,
  GetShipmentsSucess,
  GetShipmentsFailure
) => {
  return (dispatch) => {
    getCarrierShipmnets(
      requestbody,
      config,
      dispatch,
      GetShipmentsSucess,
      GetShipmentsFailure
    );
  };
};
//---------------------------------GET CUSTOMER OFFER--------------------
export const GetCustomerOffer = (
  requestbody,
  config,
  GetOfferSucess,
  GetOfferFailure
) => {
  return (dispatch) => {
    getCustomerOffer(
      requestbody,
      config,
      dispatch,
      GetOfferSucess,
      GetOfferFailure
    );
  };
};
//-------------------------ACCEPT CUSTOMER OFFER(SIMPLE)----------------------
export const AcceptSimpleCustomerOffer = (
  requestbody,
  config,
  shipment,
  AcceptOfferSuccess,
  AcceptOfferFailure
) => {
  return (dispatch) => {
    SimpleCustomerOffer(
      requestbody,
      config,
      shipment,
      dispatch,
      AcceptOfferSuccess,
      AcceptOfferFailure
    );
  };
};
//-----------------------------ACCEPT CUSTOMER OFFER(SPLITE)
export const AcceptSplitCustomerOffer = (
  requestbody,
  config,
  shipment,
  AcceptOfferSuccess,
  AcceptOfferFailure
) => {
  return (dispatch) => {
    SplitCustomerOffer(
      requestbody,
      config,
      shipment,
      dispatch,
      AcceptOfferSuccess,
      AcceptOfferFailure
    );
  };
};
//-------------------------------ADD DRIVER TRUCK(CARRIER)-----------------
export const AddTruckDriverToShipment = (
  requestbody,
  config,
  shipment,
  AddSucess,
  AddFailure
) => {
  return (dispatch) => {
    AddTruckDriver(
      requestbody,
      config,
      dispatch,
      shipment,
      AddSucess,
      AddFailure
    );
  };
}; //------------------------ADD DRIVER TRUCK TO SPLIT SHIPMENT(CARRIER)-----------------
export const AddTruckDriverToSplitShipment = (
  requestbody,
  config,
  shipment,
  AddSucess,
  AddFailure
) => {
  return (dispatch) => {
    AddTruckDriverSplit(
      requestbody,
      config,
      dispatch,
      shipment,
      AddSucess,
      AddFailure
    );
  };
};
//--------------------OFFER SEND BY CARRIER--------------------

export const CarrierOfferSend = (
  requestbody,
  config,
  offerSendSuccess,
  offerSendFailure
) => {
  return (dispatch) => {
    CarrierOfferSendService(
      requestbody,
      config,
      dispatch,
      offerSendSuccess,
      offerSendFailure
    );
  };
};
// //--------------------------Get Shipment By Id Customer---------------

export const getShipmentbyID = (shipmentid, config, onSuccessCallback) => {
  return (dispatch) => {
    getShipmentbyId(shipmentid, config, dispatch, onSuccessCallback);
  };
};
//--------------------------Get Shipment By Id Carrier---------------

export const getShipmentbyIDCarrier = (
  shipmentid,
  carrierid,
  config,
  onSuccessCallback
) => {
  return (dispatch) => {
    getShipmentbyIdCarrier(
      shipmentid,
      carrierid,
      config,
      dispatch,
      onSuccessCallback
    );
  };
};
//--------------------GET CUSTOMER NOTIFICATION----------------
export const getCustomerNotification = (
  customerid,
  config,
  onSuccessCallback
) => {
  return (dispatch) => {
    CustomerNotification(customerid, config, dispatch, onSuccessCallback);
  };
};
//--------------------GET CARRIER NOTIFICATION----------------
export const getCarrierNotification = (
  carrierid,
  config,
  onSuccessCallback
) => {
  return (dispatch) => {
    CarrierNotification(carrierid, config, dispatch, onSuccessCallback);
  };
};
//----------------------EDIT DRIVER RECORD----------
export const EditDriver = (
  requestBody,
  config,
  driverId,
  editDeleteSuccess,
  editDeleteFails
) => {
  return (dispatch) => {
    EditDriverService(
      requestBody,
      config,
      dispatch,
      driverId,
      editDeleteSuccess,
      editDeleteFails
    );
  };
};
//----------------------DELETE DRIVER RECORD----------
export const DeleteDriver = (
  config,
  driverId,
  editDeleteSuccess,
  editDeleteFails
) => {
  return (dispatch) => {
    DeleteDriverService(
      config,
      dispatch,
      driverId,
      editDeleteSuccess,
      editDeleteFails
    );
  };
};
//----------------------EDIT VEHICAL RECORD----------
export const EditVehical = (
  requestBody,
  config,
  vehicalId,
  editDeleteSuccess,
  editDeleteFails
) => {
  return (dispatch) => {
    EditVehicalService(
      requestBody,
      config,
      dispatch,
      vehicalId,
      editDeleteSuccess,
      editDeleteFails
    );
  };
};
//----------------------DELETE VEHICAL RECORD----------
export const DeleteVehical = (
  config,
  vehicalId,
  editDeleteSuccess,
  editDeleteFails
) => {
  return (dispatch) => {
    DeleteVehicalService(
      config,
      dispatch,
      vehicalId,
      editDeleteSuccess,
      editDeleteFails
    );
  };
};

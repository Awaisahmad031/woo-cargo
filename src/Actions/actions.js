//---------------------LOGIN CUSTOMER----------------------
export const LoginAction = (data) => {
  return {
    type: "LOGIN_CUSTOMER",
    data,
  };
};
//----------------------------LOGIN CARRIER--------------------------
export const LoginActionCarrier = (data) => {
  return {
    type: "LOGIN_CARRIER",
    data,
  };
};
//----------------------------SIGNUP CUSTOMER------------------------
export const SignupActionCustomer = (data) => {
  return {
    type: "SIGNUP_CUSTOMER",
    data,
  };
};
//----------------------------ADD SHIPMENTS CUSTOMER------------------
export const AddShipmentActionCustomer = (data) => {
  return {
    type: "ADD_NEW_SHIPMENT_CUSTOMER",
    data,
  };
};
//----------------------------GET SHIPMENTS----------------------------
export const GetShipmentActionCustomer = (data) => {
  return {
    type: "GET_SHIPMENT_CUSTOMER",
    data,
  };
};
// //-------------GET SHIPMENT BY ID
// export const GetSingleShipmentActionCustomer = (data) => {
//   return {
//     type: "ADD_SINGLESHIPMENT",
//     data,
//   };
// };
//----------------------------GET DRIVERS---------------------------------
export const GetVehicalsAction = (data) => {
  return {
    type: "GET_VEHICALS",
    data,
  };
};
//---------------------------ADD DRIVER-----------------------------------
export const AddDriverAction = (data) => {
  return {
    type: "ADD_DRIVER",
    data,
  };
};
//----------------------------GET VEHICALS-------------------------------
export const GetDriversAction = (data) => {
  return {
    type: "GET_DRIVERS",
    data,
  };
};
//-----------------------------ADD VEHICAL-------------------------------------
export const AddVehicalAction = (data) => {
  return {
    type: "ADD_VEHICAL",
    data,
  };
};
//--------------------------------GET CARRIER SHIPMENTS--------------------------

export const GetCarrierShipmentsAction = (data) => {
  return {
    type: "GET_CARRIER_SHIPMENTS",
    data,
  };
};

//--------------------------------GET CUSTOMER OFFER--------------------------
export const getCustomerOfferAction = (data) => {
  return {
    type: "GET_CUSTOMER_OFFER",
    data,
  };
};
//-----------------------------------ACCEPT CUSTOMER OFFER(SIPMPLE OR SPLIT)--------------
export const AcceptCustomerOfferAction = (data) => {
  return {
    type: "ACCEPT_CUSTOMER_OFFER",
    data,
  };
};

//----------------------------------CARRIER OFFER SEND------------------------
export const CarrierOfferSendAction = (data) => {
  return {
    type: "CARRIER_OFFER_SEND",
    data,
  };
};
//--------------------------ADD DRIVER TRUCK TO SHIPMENT(CARRIER)-----------------
export const AddDriverTruckToShipmentAction = (data) => {
  return {
    type: "ADD_DRIVER_TRUCK",
    data,
  };
};
//--------------------------CUSTOMER NOTIFICATION-----------------
export const CustomerNotificationAction = (data) => {
  return {
    type: "CUSTOMER_NOTIFICATION",
    data,
  };
};
//--------------------------CARRIER NOTIFICATION-----------------
export const CarrierNotificationAction = (data) => {
  return {
    type: "CARRIER_NOTIFICATION",
    data,
  };
};
//--------------------------EDIT DRIVER-----------------
export const EditDriverAction = (data, id) => {
  return {
    type: "EDIT_DRIVER",
    data,
    id,
  };
};
//--------------------------EDIT VEHICAL-----------------
export const EditVehicalAction = (data, id) => {
  return {
    type: "EDIT_VEHICAL",
    data,
    id,
  };
};
//--------------------------DELETE DRIVER-----------------
export const DeleteDriverAction = (data) => {
  return {
    type: "DELETE_DRIVER",
    data,
  };
};
//--------------------------DELETE VEHICAL-----------------
export const DeleteVehicalAction = (data) => {
  return {
    type: "DELETE_VEHICAL",
    data,
  };
};

import React from "react";
import logo from "../Pics/logo_with_text.svg";
import Joi from "joi-browser";
import InputField from "../Components/Common/InputField";
import SignupBox from "../Components/Common/SignupBox";
import Form from "../Components/Common/Form";
import { CustomerSignup } from "../Actions/actioncreators";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { createLocalStorageCustomer } from "../LocalStorage/Auth";
import "../Styles/LogInForm.css";

class SignUp extends Form {
  state = {
    data: {
      customer_name: "",
      customer_mobilenum: "",
      customer_email: "",
      customer_password: "",
    },
    error: {},
    invalid_signup: false,
    signupButtonLoading: false,
  };
  schema = {
    customer_name: Joi.string().required().label("Name"),
    customer_mobilenum: Joi.string().required().label("Mobile No."),
    customer_email: Joi.string()
      .email({ minDomainAtoms: 2 })
      .required()
      .label("Email"),
    customer_password: Joi.string().required().min(6).label("Password"),
  };
  spinnerButton = () => {
    this.setState({ signupButtonLoading: true });
  };
  // After clicking the submit button of signup
  customerSignup = (e) => {
    e.preventDefault();
    this.spinnerButton();
    // validation of input fields
    const error = this.validate();
    this.setState({ error: error || {} });
    //if error occur don't call api
    if (error) return null;

    //call the api
    this.callServerApiSignup();
  };
  callServerApiSignup = () => {
    this.setState({ invalid_signup: false });
    const requestBody = {
      customer_name: this.state.data.customer_name,
      customer_mobilenum: this.state.data.customer_mobilenum,
      customer_email: this.state.data.customer_email,
      customer_password: this.state.data.customer_password,
    };

    const config = {
      headers: {
        "Content-Type": "application/json",
      },
    };
    this.props.signupCustomer(
      requestBody,
      config,
      this.signupSucessCustomer,
      this.signupFailure
    );
  };
  signupSucessCustomer = (token, obj) => {
    createLocalStorageCustomer(token, obj);
    this.props.history.push("/Shipments");
  };
  signupFailure = () => {
    console.log("sign up fails");
    this.setState({ invalid_signup: true, signupButtonLoading: false });
  };

  render() {
    return (
      <div className="container-fluid" style={{ background: "#ffffff" }}>
        <div className="row">
          <div className="logo--position">
            <img src={logo} className="img-fluid" alt="logo"></img>
          </div>

          <div className="col-md-4 offset-md-1 top__signup--margin form__padding--smallscreen">
            <span className="fontchange">Get Started</span>
            <small className="text--color d-block">
              Enter in a new World of Shipments.
            </small>
            {/*-----------BOXES---------------  */}
            <div className="boxes my-3">
              <SignupBox title="Customer" />
            </div>
            {/* --------Duplicate Email Error---------- */}
            {this.state.invalid_signup && (
              <div className="alert alert-danger">
                This Email Address already has been taken.
              </div>
            )}

            {/* ------------===form start------------------- */}
            <form onSubmit={this.customerSignup}>
              <div className="form-group">
                <InputField
                  labeltitle="FULL NAME"
                  typename="text"
                  fieldname="customer_name"
                  changeHandler={this.handleChange}
                  value={this.state.data.customer_name}
                  error={this.state.error.customer_name}
                />
              </div>
              <div className="form-group">
                <InputField
                  labeltitle="MOBILE PHONE"
                  typename="text"
                  fieldname="customer_mobilenum"
                  changeHandler={this.handleChange}
                  value={this.state.data.customer_mobilenum}
                  error={this.state.error.customer_mobilenum}
                />
              </div>
              <div className="form-group">
                <InputField
                  labeltitle="EMAIL"
                  typename="email"
                  fieldname="customer_email"
                  changeHandler={this.handleChange}
                  value={this.state.data.customer_email}
                  error={this.state.error.customer_email}
                />
              </div>
              <div className="form-group">
                <InputField
                  labeltitle="PASSWORD"
                  typename="password"
                  fieldname="customer_password"
                  changeHandler={this.handleChange}
                  value={this.state.data.customer_password}
                  error={this.state.error.customer_password}
                />
              </div>
              <button
                disabled={this.validate() || this.state.signupButtonLoading}
                type="submit"
                className="btn btn-primary btncolor btn-block btn-lg"
              >
                {this.state.signupButtonLoading ? (
                  <span>Loading ...</span>
                ) : (
                  <span>Create Account</span>
                )}
              </button>
              <div className="text--color text-center mt-2 my-4">
                Already have an account &nbsp;
                <Link to="/">
                  <span className="font-weight-bold">Sign in</span>
                </Link>
              </div>
            </form>
            {/*----------------Form end-------------- */}
          </div>
          <div className="col-md-6 offset-md-1 pr-0">
            <div className="bgpic"></div>
          </div>
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    signupCustomer: (
      requestBody,
      config,
      signupSucessCustomer,
      signupFailure
    ) => {
      dispatch(
        CustomerSignup(requestBody, config, signupSucessCustomer, signupFailure)
      );
    },
  };
};
export default connect(null, mapDispatchToProps)(SignUp);

import React from "react";
import AddButton from "../../Components/Common/AddButtonCarr";
import Empty from "../../Components/Common/EmptyCarrier";
import Modal from "react-bootstrap/Modal";
import DriverCard from "../../Components/CarrierSide/DriverCard";
import NavBarCarrier from "../../Components/Common/NavbarCarrier";
import DriveApiCall from "../../Components/CarrierSide/DriveApiCall";
import AddDriverModal from "../../Components/CarrierSide/AddDriverModal";
import { connect } from "react-redux";
import { Circle2 } from "react-preloaders";
import { GetDriversCarrierSide } from "../../Actions/actioncreators";

class DriverDashboard extends DriveApiCall {
  state = {
    record: [],
    loading: true,
    modal: false,
  };
  componentDidMount() {
    if (this.props.carrier_AllDrivers.length > 0) {
      return this.setState({
        record: this.props.carrier_AllDrivers,
        loading: false,
      });
    }
    this.requestToLocalStorage();
  }
  handleModal = () => {
    this.setState({ modal: !this.state.modal });
  };

  render() {
    console.log("modal", this.state.modal);
    const { loading } = this.state;
    const count = this.state.record.length;
    if (loading) {
      return <Circle2 customLoading={loading} time={0} />;
    } else if (count === 0) {
      return <Empty title="Driver Dashboard" />;
    } else {
      return (
        <React.Fragment>
          <NavBarCarrier />
          <div
            style={{
              height: "90vh",
              overflowY: "auto",
              overflowX: "none",
              msOverflowStyle: "none",
            }}
          >
            <div className="container pt-5">
              <div className="row">
                <div className="col-md-10">
                  <span className="vd-title">Driver Dashboard</span>
                  <span className="vd-total ml-3">{count} Total</span>
                  <div>
                    <div className="container mt-5">
                      <div className="row">
                        <div className="col-3">
                          <span className="numbercolor">NAME</span>
                        </div>
                        <div className="col-3">
                          <span className="numbercolor">PHONE NUMBER</span>
                        </div>
                        <div className="col-4">
                          <span className="numbercolor">EMAIL</span>
                        </div>
                      </div>
                    </div>
                    <div
                      style={{
                        height: "70vh",
                        overflowX: "none",
                        overflowY: "auto",
                        msOverflowStyle: "none",
                      }}
                    >
                      {this.state.record.map((item) => {
                        return <DriverCard key={item._id} list={item} />;
                      })}
                    </div>
                  </div>
                </div>
                <div className="col-2">
                  <div onClick={this.handleModal}>
                    <AddButton />
                  </div>
                  <Modal
                    size="lg"
                    show={this.state.modal}
                    onHide={this.handleModal}
                    backdrop="static"
                    animation="true"
                    aria-labelledby="example-modal-sizes-title-lg"
                  >
                    <AddDriverModal
                      handleModal={this.handleModal}
                      title="AddDriver"
                    />
                  </Modal>
                </div>
              </div>
            </div>
          </div>
        </React.Fragment>
      );
    }
  }
}
const mapStateToProps = (state) => {
  return {
    carrier_AllDrivers: state.driverReducer.Drivers,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    getAllDrivers: (requestBody, config, GetDriversSucess, GetDriversFailure) =>
      dispatch(
        GetDriversCarrierSide(
          requestBody,
          config,
          GetDriversSucess,
          GetDriversFailure
        )
      ),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(DriverDashboard);

import React from "react";
import VehicalCard from "../../Components/CarrierSide/VehicalCard";
import VehicalApiCall from "../../Components/CarrierSide/VehicalApiCall";
import Empty from "../../Components/Common/EmptyCarrier";
import AddButton from "../../Components/Common/AddButtonCarr";
import NavBarCarrier from "../../Components/Common/NavbarCarrier";
import Modal from "react-bootstrap/Modal";
import AddVehicalModal from "../../Components/CarrierSide/AddVehicalModal";
import { connect } from "react-redux";
import { Circle2 } from "react-preloaders";
import { GetVehicalsCarrierSide } from "../../Actions/actioncreators";
import "../../Styles/CarrierDashboard.css";

class VehicalDashboard extends VehicalApiCall {
  constructor(props) {
    super(props);
    this.state = {
      record: [],
      loading: true,
      modal: false,
    };
  }
  handleModal = () => {
    this.setState({ modal: !this.state.modal });
  };
  componentDidMount() {
    if (this.props.carrier_AllVehicals.length > 0) {
      return this.setState({
        record: this.props.carrier_AllVehicals,
        loading: false,
      });
    }
    this.requestToLocalStorage();
  }
  render() {
    const { loading } = this.state;
    const count = this.state.record.length;
    if (loading) {
      return <Circle2 customLoading={loading} time={0} />;
    } else if (count === 0) {
      return <Empty title="Vehicle Dashboard" />;
    } else {
      return (
        <React.Fragment>
          <NavBarCarrier />
          <div
            style={{
              height: "90vh",
              overflowY: "auto",
              overflowX: "none",
              msOverflowStyle: "none",
            }}
          >
            <div className="container pt-5">
              <div className="row">
                <div className="col-md-10">
                  <span className="vd-title">Vehicles Dashboard</span>
                  <span className="vd-total ml-3">{count} Total</span>
                  <div>
                    {/*--------------title start---------- */}
                    <div className="container mt-5">
                      <div className="row">
                        <div className="col-3">
                          <span className="numbercolor">NAME AND ID</span>
                        </div>
                        <div className="col-2">
                          <span className="numbercolor">VEHICLE MODEL</span>
                        </div>
                        <div className="col-1">
                          <span className="numbercolor">TYPE</span>
                        </div>
                        <div className="col-2">
                          <span className="numbercolor">CAPACITY</span>
                        </div>
                        <div className="col-2">
                          <span className="numbercolor">WEIGHT</span>
                        </div>
                      </div>
                    </div>
                    {/*--------------Cards start---------- */}
                    <div
                      style={{
                        height: "70vh",
                        overflowX: "none",
                        overflowY: "auto",
                        msOverflowStyle: "none",
                      }}
                    >
                      {this.state.record.map((item, index) => {
                        return <VehicalCard key={index} list={item} />;
                      })}
                    </div>
                    {/*--------------Cards end---------- */}
                  </div>
                </div>
                {/*--------------side plus button---------- */}
                <div className="col-2">
                  <div onClick={this.handleModal}>
                    <AddButton />
                  </div>
                  <Modal
                    size="lg"
                    show={this.state.modal}
                    onHide={this.handleModal}
                    backdrop="static"
                    animation="true"
                    aria-labelledby="example-modal-sizes-title-lg"
                  >
                    <AddVehicalModal
                      handleModal={this.handleModal}
                      title="AddVehical"
                    />
                  </Modal>
                </div>
              </div>
            </div>
          </div>
        </React.Fragment>
      );
    }
  }
}
const mapStateToProps = (state) => {
  return {
    carrier_AllVehicals: state.vehicalReducer.Vehicals,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    getAllVehicals: (
      requestBody,
      config,
      GetVehicalSucess,
      GetVehicalFailure
    ) =>
      dispatch(
        GetVehicalsCarrierSide(
          requestBody,
          config,
          GetVehicalSucess,
          GetVehicalFailure
        )
      ),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(VehicalDashboard);

import React from "react";
import { connect } from "react-redux";
import { Circle2 } from "react-preloaders";
import { GetShipmentsCarrierSide } from "../../Actions/actioncreators";
import CardTitle from "../../Components/Common/CardTitle";
import ApiCallCarrierShipment from "../../Components/CarrierSide/ApiCallCarrierShipment";
import Empty from "../../Components/Common/EmptyCarrier";
import InputSlider from "../../Components/Common/InputSlider";
import NavBarCarrier from "../../Components/Common/NavbarCarrier";
import Card from "../../Components/Common/Card";
import "../../Styles/CustomerHome.css";

class SearchShipment extends ApiCallCarrierShipment {
  constructor(props) {
    super(props);
    this.state = {
      data: {
        pickuplocation: "",
        deliverylocation: "",
        type: "",
      },
      record: [],
      filterdata: [],
      filtermode: false,
      alldatashow: true,
      loading: true,
    };
  }

  changeHandle = ({ target }) => {
    const data = { ...this.state.data };
    data[target.name] = target.value;
    this.setState({ data });
  };
  componentDidMount() {
    if (this.props.carrier_AllShipments.length > 0) {
      const record = this.props.carrier_AllShipments.filter((shipment) => {
        return shipment.offer_accepted === undefined;
      });
      return this.setState({
        record,
        loading: false,
      });
    }
    this.requestToLocalStorage();
  }

  // search record by filter just based on pickup and delivery location
  searchRecord = (e) => {
    e.preventDefault();
    let filterRecord = this.state.record.filter((item) => {
      return (
        item.pickup_location.address.toLowerCase() ===
          this.state.data.pickuplocation.trim().toLowerCase() &&
        item.dropoff_location.address.toLowerCase() ===
          this.state.data.deliverylocation.trim().toLowerCase()
      );
    });
    this.setState({
      filterdata: filterRecord,
      filtermode: true,
      alldatashow: false,
    });
  };

  render() {
    const { loading, alldatashow } = this.state;
    const count = this.state.record.length;
    const countF = this.state.filterdata.length;
    if (loading) {
      return <Circle2 customLoading={loading} time={0} />;
    } else if (count === 0) {
      return <Empty title="Search Shipments" />;
    } else {
      return (
        <React.Fragment>
          <NavBarCarrier />
          <div className="container-fluid">
            <div className="container-fluid pt-5">
              <div className="row">
                {/* heading of card start*/}
                <div className="col-md-8 offset-md-1">
                  <span className="vd-title">Search Shipments</span>
                  <span className="vd-total ml-3">
                    {alldatashow ? count : countF} Total
                  </span>
                  <div
                    style={{
                      overflowY: "auto",
                      overflowX: "none",
                      height: "75vh",
                      msOverflowStyle: "none",
                    }}
                  >
                    {/* heading of card end */}
                    <CardTitle />
                    {/* all data show */}

                    {this.state.alldatashow &&
                      this.state.record.map((item, index) => {
                        return (
                          <Card
                            key={index}
                            list={item}
                            detailPage="CarrierHome"
                          />
                        );
                      })}

                    {/* filter data */}
                    {this.state.filtermode &&
                      this.state.filterdata.map((item, index) => {
                        return (
                          <Card
                            key={index}
                            list={item}
                            detailPage="CarrierHome"
                          />
                        );
                      })}
                  </div>
                </div>
                <div
                  id="filter_module"
                  className="col-md-3 pt-4 sizeofmodule shadow-sm"
                  style={{ background: "#fff" }}
                >
                  <span
                    style={{ alignSelf: "flex-start", marginBottom: 10 }}
                    className="red_text"
                  >
                    FILTER SHIPMENTS
                  </span>
                  <form className="mt-1" onSubmit={this.searchRecord}>
                    <div className="form-group">
                      {this.renderInput("Pickup Location", "pickuplocation")}
                    </div>
                    {/* input range */}
                    <div>
                      <label className="filter-title">Distance</label>
                      <InputSlider />
                    </div>

                    <div className="form-group mt-1">
                      {this.renderInput(
                        "Delivery Location",
                        "deliverylocation"
                      )}
                    </div>
                    <div>
                      <label className="filter-title">Distance</label>
                      <InputSlider />
                    </div>
                    <div className="form-group mt-1">
                      {this.renderInput("Type", "type")}
                    </div>
                    <button className="btn btn-primary btn-block" type="submit">
                      Search
                    </button>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </React.Fragment>
      );
    }
  }
}
const mapStateToProps = (state) => {
  return {
    carrier_AllShipments: state.carrierReducer.Carrier_Shipments,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    getCarrierShipments: (
      requestBody,
      config,
      GetShipmentsSucess,
      GetShipmentsFailure
    ) =>
      dispatch(
        GetShipmentsCarrierSide(
          requestBody,
          config,
          GetShipmentsSucess,
          GetShipmentsFailure
        )
      ),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(SearchShipment);

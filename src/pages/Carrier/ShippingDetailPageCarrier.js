import React from "react";
import NavBarCarrier from "../../Components/Common/NavbarCarrier";
import LocationIcon from "../../Components/Common/LocationIcon";
import AddDriverTruckAPI from "../../Components/CarrierSide/AddDriverTruckAPI";
import DriverSelector from "../../Components/Common/DriverSelector";
import TruckSelector from "../../Components/Common/TruckSelector";
import RowRemoval from "../../Components/CarrierSide/RowRemoval";
import RowAttributes from "../../Components/CarrierSide/RowAttributes";
import { Link } from "react-router-dom";
import { Circle2 } from "react-preloaders";
import { connect } from "react-redux";
import { LoginActionCarrier } from "../../Actions/actions";
import {
  AddTruckDriverToShipment,
  AddTruckDriverToSplitShipment,
  GetDriversCarrierSide,
  GetVehicalsCarrierSide,
  getShipmentbyIDCarrier,
} from "../../Actions/actioncreators";

class ShippingDetailPageCarrier extends AddDriverTruckAPI {
  state = {
    shipmentid: this.props.match.params.id,
    shipmentobj: {},
    allTrucks: [],
    allDrivers: [],
    truck: "",
    driver: "",
    loading: true,
    addedSuccess: false,
  };

  //--ADD TRUCK AND DRIVER TO STATE
  componentDidMount() {
    this.GetShipmentById();
    this.GetAllVehicals();
    this.GetAllDrivers();
  }

  renderDescription(title, value, col = "col-2") {
    return (
      <div className={col}>
        <span className="offer-description text-uppercase d-block">
          {title}
        </span>
        <span className="offer-notes">{value}</span>
      </div>
    );
  }

  render() {
    const { loading } = this.state;
    if (loading) return <Circle2 customLoading={loading} time={0} />;
    const { shipmentobj } = this.state;
    const {
      _id,
      shipment_title,
      shipment_attributes,
      shipment_type,
      pickup_location,
      dropoff_location,
      pickup_date,
      dropoff_date,
      price,
      shipment_description,
    } = shipmentobj;
    const pickupdate = pickup_date.slice(0, 10);
    const dropoffdate = dropoff_date.slice(0, 10);
    return (
      <React.Fragment>
        <div
          style={{
            overflowY: "auto",
            height: "100vh",
            overflowX: "hidden",
            msOverflowStyle: "none",
          }}
        >
          <NavBarCarrier />
          <div className="container mb-4">
            <div className="heading my-4">
              <h2 className="d-inline-block">SHIPMENT INFORMATION</h2>
            </div>
            <div
              className="container shadow-sm"
              style={{ background: "#ffffff" }}
            >
              <div className="p-2 pt-4">
                <span className="text-uppercase text-muted">{shipmentobj.shipment_status}</span>
                <div className="container shadow-sm mt-3">
                  <div className="pt-3 p-3">
                    <div className="row">
                      <div className="container">
                        <div className="float-left">
                          <span className="offer-description d-block">{shipment_description}</span>
                          <span className="offer-notes">
                            Notes...
                          </span>
                        </div>
                        <div className="float-right">
                          <span
                            className="d-block"
                            style={{ color: "#2e5bff", fontSize: "30px" }}
                          >
                           {price==null? '-' : price}$
                          </span>
                          <span
                            className="d-block text-muted"
                            style={{ fontSize: "18px" }}
                          >
                            {shipment_type}
                          </span>
                        </div>
                      </div>
                      {/* Just Location icon */}
                      <LocationIcon />
                      <div className="col-6">
                        <span className="offer-description d-block mt-1">
                          {pickup_location.address}
                        </span>
                        <span className="offer-notes d-block">
                          {pickupdate}
                        </span>
                         <span className="offer-description">Name</span>
                      <span className="offer-notes ml-2">shippername</span>
                      <div>
                        <span className="offer-description">Tel.</span>
                        <span className="offer-notes ml-4">shippertel</span>
                      </div> 
                         <span className="offer-notes d-block">pickuptime</span> 
                        <span className="offer-description">Mob.</span>
                        <span className="offer-notes ml-2">shippermob</span>
                      </div>
                      <div className="col-6">
                        <span className="offer-description d-block mt-1">
                          {dropoff_location.address}
                        </span>
                        <span className="offer-notes d-block">
                          {dropoffdate}
                        </span>
                        
                      <span className="offer-description">Name</span>
                      <span className="offer-notes ml-2">receivername</span>
                      <div>
                        <span className="offer-description">Tel.</span>
                        <span className="offer-notes ml-4">receivertel</span>
                      </div> 
                         <span className="offer-notes d-block">
                          deliverytime
                        </span> 
                        <span className="offers-description">Mob.</span>
                        <span className="offer-notes ml-2">receivermob</span>
                      </div>
                    </div>
                    <hr />
                    {/*----DELIVERY DESCRIPTION */}

                    {shipment_type === "REMOVAL" ? (
                      <RowRemoval
                        type={shipment_type}
                        data={shipment_attributes}
                      />
                    ) : (
                      <RowAttributes
                        type={shipment_type}
                        data={shipment_attributes}
                      />
                    )}

                    <hr />
                    {/* comment box */}
                    <div className="mt-4">
                      <textarea
                        className="form-control"
                        rows="5"
                        id="comment"
                        style={{ border: " 0.5px dashed #777777" }}
                      ></textarea>
                      <span className="offer-notes d-block">Notes</span>
                    </div>
                  </div>
                </div>
                {/* form */}
                {/*-------if truck and driver already added------- */}
                {shipmentobj.selected_driver && shipmentobj.selected_truck ? (
                  null
                ) : (
                  <div className="mt-5">
                    <p>
                      Please Input your Truck and Driver or start your Shipment
                    </p>
                    <form onSubmit={this.formSubmit} className="mt-2">
                      <div className="form-row">
                        <div className="form-group col-3">
                          <DriverSelector
                            labelTitle="Driver"
                            name="driver"
                            Option={this.state.allDrivers}
                            value={this.state.driver}
                            changeHandler={this.changeHandler}
                          />
                        </div>
                        <div className="form-group col-3">
                          <TruckSelector
                            labelTitle="Truck"
                            name="truck"
                            Option={this.state.allTrucks}
                            value={this.state.truck}
                            changeHandler={this.changeHandler}
                          />
                        </div>
                        <div className="form-group col-3"></div>
                      </div>
                      <div className="form-row mt-2">
                        <div className="form-group col-3">
                          <button
                            className="btn btn-primary btn-block"
                            type="submit"
                          >
                            Add
                          </button>
                        </div>
                        <div className="form-group col-3">
                          <Link
                            to="/CarrierHome"
                            style={{ textDecoration: "none" }}
                          >
                            <button className="btn btn-primary btn-block">
                              Back
                            </button>
                          </Link>
                        </div>
                      </div>
                    </form>
                  </div>
                )}
              </div>
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    authToken: state.carrierReducer.auth_Token,
    Carrier_AllShipment: state.carrierReducer.Carrier_Shipments,
    Drivers: state.driverReducer.Drivers,
    Trucks: state.vehicalReducer.Vehicals,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    addDriverTruck: (requestbody, config, shipment, addSuccess, addFailure) =>
      dispatch(
        AddTruckDriverToShipment(
          requestbody,
          config,
          shipment,
          addSuccess,
          addFailure
        )
      ),
    addDriverTruckSplit: (
      requestbody,
      config,
      shipment,
      addSuccess,
      addFailure
    ) =>
      dispatch(
        AddTruckDriverToSplitShipment(
          requestbody,
          config,
          shipment,
          addSuccess,
          addFailure
        )
      ),
    getAllDrivers: (requestBody, config, GetDriversSucess, GetDriversFailure) =>
      dispatch(
        GetDriversCarrierSide(
          requestBody,
          config,
          GetDriversSucess,
          GetDriversFailure
        )
      ),
    getAllVehicals: (
      requestBody,
      config,
      GetVehicalSucess,
      GetVehicalFailure
    ) =>
      dispatch(
        GetVehicalsCarrierSide(
          requestBody,
          config,
          GetVehicalSucess,
          GetVehicalFailure
        )
      ),
    addCarrierDetails: (authToken, carrier) =>
      dispatch(LoginActionCarrier({ authToken, carrier })),
    getShipmentbyId: (shipmentid, carrierid, config, onSuccessCallback) =>
      dispatch(
        getShipmentbyIDCarrier(shipmentid, carrierid, config, onSuccessCallback)
      ),
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ShippingDetailPageCarrier);

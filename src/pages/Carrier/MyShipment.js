import React, { Component } from "react";
import { connect } from "react-redux";
import Card from "../../Components/Common/Card";
import { Circle2 } from "react-preloaders";
import CardTitle from "../../Components/Common/CardTitle";
import Empty from "../../Components/Common/EmptyCarrier";
import { getCarrier, getCarrierUserId } from "../../LocalStorage/Auth";
import { GetShipmentsCarrierSide } from "../../Actions/actioncreators";
import FilterModule from "../../Components/Common/FilterModule";
import NavBarCarrier from "../../Components/Common/NavbarCarrier";
import "../../Styles/CustomerHome.css";
let buttontext = [
  { id: 1, text: "All Shipments" },
  { id: 2, text: "In progress" },
  { id: 3, text: "Upcoming" },
  { id: 4, text: "Finished" },
  { id: 5, text: "Waiting" },
];

class MyShipment extends Component {
  state = {
    record: [],
    filterdata: [],
    Allshipment: true,
    Filtershipment: false,
    loading: true,
    button_select: "All Shipments",
  };
  //-----FILTER RECORDS--------
  onPressButton = (text) => {
    if (text === "All Shipments") {
      return this.setState({
        Allshipment: true,
        Filtershipment: false,
        button_select: text,
      });
    } else this.filterFunction(text);
  };
  //FILTER RECORD
  filterFunction = (text) => {
    const filterdata = this.state.record.filter((item) => {
      return item.shipment_status.trim() === text.toUpperCase();
    });
    this.setState({
      filterdata,
      Allshipment: false,
      Filtershipment: true,
      button_select: text,
    });
  };

  componentDidMount() {
    if (this.props.carrier_AllShipments.length > 0) {
      const { id: carrierId } = getCarrierUserId();
      const record = this.props.carrier_AllShipments.filter((shipment) => {
        return shipment.selected_carrier === carrierId;
      });
      return this.setState({
        record,
        loading: false,
      });
    }
    this.getMyShipments();
  }
  //--------------------Api call--------------------
  getMyShipments = () => {
    const token = getCarrier();
    const { id } = getCarrierUserId();
    const requestBody = {
      carrierid: id,
    };
    const config = {
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
      },
    };
    this.props.getCarrierShipments(
      requestBody,
      config,
      this.GetShipmentsSucess,
      this.GetShipmentsFailure
    );
  };
  GetShipmentsFailure = () => {
    console.log("Fail get shipments");
  };
  GetShipmentsSucess = () => {
    const { id: carrierId } = getCarrierUserId();
    const record = this.props.carrier_AllShipments.filter((shipment) => {
      return shipment.selected_carrier === carrierId;
    });
    this.setState({ record, loading: false });
  };
  render() {
    const { loading, Allshipment, filterdata, record } = this.state;
    const count = record.length;
    const countF = filterdata.length;
    if (loading) {
      return <Circle2 customLoading={loading} time={0} />;
    } else if (count === 0) {
      return <Empty title="My Shipments" />;
    } else {
      return (
        <React.Fragment>
          <NavBarCarrier />
          <div className="container-fluid">
            <div className="container-fluid pt-5">
              <div className="row">
                {/* ------heading of card start--------*/}
                <div className="col-md-8 offset-md-1">
                  <span className="vd-title">My Shipments</span>
                  <span className="vd-total ml-3">
                    {Allshipment ? count : countF} Total
                  </span>
                  <div
                    style={{
                      overflowY: "auto",
                      overflowX: "none",
                      height: "75vh",
                      msOverflowStyle: "none",
                    }}
                  >
                    {/*--------dont show when no filter data found-------- */}
                    {Allshipment || filterdata.length > 0 ? (
                      <CardTitle />
                    ) : null}
                    {/*------------all data show--------- */}
                    {Allshipment &&
                      record.map((item, index) => {
                        return (
                          <Card
                            key={index}
                            list={item}
                            detailPage="CarrierHome"
                          />
                        );
                      })}
                    {/*----------filter data---------- */}
                    {this.state.Filtershipment ? (
                      filterdata.length > 0 ? (
                        filterdata.map((item, index) => {
                          return (
                            <Card
                              key={index}
                              list={item}
                              detailPage="CarrierHome"
                            />
                          );
                        })
                      ) : (
                        <p className="mt-5">No Shipments found!</p>
                      )
                    ) : null}
                  </div>
                </div>
                {/*----------SEARCH FILTER--------- */}
                <div className="col-3">
                  <div
                    className="sizeofmodule shadow-sm"
                    style={{ background: "#ffffff" }}
                  >
                    <span
                      style={{ alignSelf: "flex-start", marginBottom: 10 }}
                      className="light_gray_text"
                    >
                      FILTER SHIPMENTS
                    </span>
                    {/*---------------BUTTONS---------------------- */}
                    {buttontext.map((item) => {
                      return (
                        <FilterModule
                          key={item.id}
                          button_select={this.state.button_select}
                          onPressButton={this.onPressButton}
                          buttonText={item.text}
                        />
                      );
                    })}
                  </div>
                </div>
              </div>
            </div>
          </div>
        </React.Fragment>
      );
    }
  }
}
const mapStateToProps = (state) => {
  return {
    carrier_AllShipments: state.carrierReducer.Carrier_Shipments,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    getCarrierShipments: (
      requestBody,
      config,
      GetShipmentsSucess,
      GetShipmentsFailure
    ) =>
      dispatch(
        GetShipmentsCarrierSide(
          requestBody,
          config,
          GetShipmentsSucess,
          GetShipmentsFailure
        )
      ),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(MyShipment);

import React, { Component } from "react";
import ChatShipment from "../../Components/CustomerSide/ChatShipment";
import ChatArea from "../../Components/CustomerSide/ChatArea";
import NavBar from "../../Components/Common/NavBar";

export default class ChatWithCarrier extends Component {
  state = { roomselected: "" };
  componentDidMount() {}
  selectroom = (selectedroom) => {
    console.log(selectedroom, "roomselectedforchat");
    this.setState({ roomselected: selectedroom });
  };
  render() {
    return (
      <React.Fragment>
        <NavBar />
        <div className="container">
          <div className="row mt-5">
            <div className="col-3 mr-0 pr-0">
              <ChatShipment selectroomcallback={this.selectroom} />
            </div>
            <div className="col-9 ml-0 pl-0">
              <ChatArea roomselected={this.state.roomselected} />
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

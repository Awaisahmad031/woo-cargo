import React, { Component } from "react";
import NavBar from "../../Components/Common/NavBar";
import TypesOfShipments from "../../Components/CustomerSide/TypesOfShipments";
import LTL from "../../Components/CustomerSide/ShipmentTypes/LTL";
import FTL from "../../Components/CustomerSide/ShipmentTypes/FTL";
import Dryage from "../../Components/CustomerSide/ShipmentTypes/Dryage";
import Removals from "../../Components/CustomerSide/ShipmentTypes/Removals";
import Car from "../../Components/CustomerSide/ShipmentTypes/Car";
export default class NewShipment extends Component {
  state = {
    shipment_type: "",
    shipment_title: "",
  };
  // Select type of shipment
  shipmentType = (item) => {
    this.setState({
      shipment_type: item.shipment_type,
      shipment_title: item.shipment_title,
    });
  };
  render() {
    return (
      <React.Fragment>
        <div
          style={{
            overflowY: "auto",
            height: "100vh",
            overflowX: "hidden",
            msOverflowStyle: "none",
          }}
        >
          <NavBar />
          <div className="container-fluid">
            <div className="container pt-5 mb-4">
              <h2>New Shipments</h2>
              <TypesOfShipments
                shipmentType={this.shipmentType}
                state={this.state}
              />
            </div>

            {/*----------LTL SHIPMENT----------- */}
            {this.state.shipment_type === "LTL" ? (
              <LTL shipmentInfo={this.state} />
            ) : null}
            {/*----------FTL SHIPMENT----------- */}
            {this.state.shipment_type === "FTL" ? (
              <FTL shipmentInfo={this.state} />
            ) : null}
            {/*----------DRYAGE SHIPMENT----------- */}
            {this.state.shipment_type === "DRYAGE" ? (
              <Dryage shipmentInfo={this.state} />
            ) : null}
            {/*----------REMOVALS SHIPMENT----------- */}
            {this.state.shipment_type === "REMOVAL" ? (
              <Removals shipmentInfo={this.state} />
            ) : null}
            {/*----------CAR SHIPMENT----------- */}
            {this.state.shipment_type === "CARS & BOATS" ? (
              <Car shipmentInfo={this.state} />
            ) : null}
          </div>
        </div>
      </React.Fragment>
    );
  }
}

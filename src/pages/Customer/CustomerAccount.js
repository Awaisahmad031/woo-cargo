import React, { Component } from "react";
import { connect } from "react-redux";
import NavBar from "../../Components/Common/NavBar";
import PersonalData from "../../Components/CustomerSide/PersonalData";
import ChangePassword from "../../Components/CustomerSide/ChangePassword";

class CustomerAccount extends Component {
  render() {
    return (
      <div
        style={{
          height: "100vh",
          overflowY: "auto",
          overflowX: "none",
          msOverflowStyle: "none",
        }}
      >
        <NavBar />
        <div className="container-fluid">
          <div className="container pt-5">
            <h2 className="mb-4">My Account</h2>
            <PersonalData customer={this.props.customerObj} />
            <ChangePassword />
          </div>
        </div>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    customerObj: state.customerReducer.Customer,
  };
};

export default connect(mapStateToProps, null)(CustomerAccount);

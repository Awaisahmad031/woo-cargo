import React, { Component } from "react";
import NavBar from "../../Components/Common/NavBar";
import FilterModule from "../../Components/Common/FilterModule";
import Card from "../../Components/Common/Card";
import CardTitle from "../../Components/Common/CardTitle";
import EmptyCusShip from "../../Components/Common/EmptyCusShip";
import { GetShipmentCustomerSide } from "../../Actions/actioncreators";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { Lines } from "react-preloaders";
import {
  getCustomerUserId,
  getCustomer,
  getCustomerObj,
} from "../../LocalStorage/Auth";
import "../../Styles/CustomerHome.css";
import { LoginAction } from "../../Actions/actions";

let buttontext = [
  { id: 1, text: "All Shipments" },
  { id: 2, text: "In progress" },
  { id: 3, text: "Upcoming" },
  { id: 4, text: "Finished" },
  { id: 5, text: "Waiting" },
];

class Shipments extends Component {
  state = {
    data: [],
    filterRecord: [],
    Allshipment: true,
    Filtershipment: false,
    button_select: "All Shipments",
    progressvalue: 0,
    loading: true,
  };
  //-----FILTER RECORDS--------
  onPressButton = (text) => {
    if (text === "All Shipments") {
      return this.setState({
        Allshipment: true,
        Filtershipment: false,
        button_select: text,
      });
    } else this.filterFunction(text);
  };
  //FILTER RECORD
  filterFunction = (text) => {
    const filterRecord = this.state.data.filter((item) => {
      return item.shipment_status.trim() === text.toUpperCase();
    });
    this.setState({
      filterRecord,
      Allshipment: false,
      Filtershipment: true,
      button_select: text,
    });
  };

  componentDidMount() {
    const token = getCustomer();
    const customer = getCustomerObj();
    const { id } = getCustomerUserId();

    this.props.addCustomerDetails(token, customer);

    const requestBody = {
      customerid: id,
    };
    const config = {
      headers: {
        Authorization: `Bearer ${token}`,
        "Content-Type": "application/json",
      },
    };
    if (
      this.props.customer_AllShipment.length === 0 ||
      this.props.customer_AllShipment.length === 1
    ) {
      this.props.getAllShipment(
        requestBody,
        config,
        this.GetShipmentSucess,
        this.GetShipmentFailure
      );
    }
    this.setState({ data: this.props.customer_AllShipment, loading: false });
  }
  GetShipmentSucess = () => {
    console.log("Successfully Fetch ");
    const data = this.props.customer_AllShipment;
    console.log(data, "this is data to be set in state");
    this.setState({ data, loading: false });
  };
  GetShipmentFailure = () => {
    console.log("Fail to Fetch ");
  };

  render() {
    const { loading, filterRecord, data } = this.state;
    const count = data.length;
    const filterCount = filterRecord.length;

    if (loading) {
      return (
        <Lines
          color="black"
          background="#f4f6fc"
          time={0}
          customLoading={loading}
        />
      );
    } else if (count === 0) {
      return <EmptyCusShip />;
    } else {
      return (
        <React.Fragment>
          <NavBar />
          <div className="container-fluid" style={{ minHeight: "90vh" }}>
            <div className="container-fluid pt-5">
              <div className="row">
                {/* heading of card start*/}
                <div className="col-md-8 offset-md-1">
                  <h2 className="d-inline">My Shipments</h2>
                  <span className="vd-total ml-3">
                    {this.state.Allshipment ? count : filterCount}
                    Total
                  </span>
                  <div className='hideoverflow'>
                    {/*--------dont show when no filter data found-------- */}
                    {this.state.Allshipment || filterRecord.length > 0 ? (
                      <CardTitle />
                    ) : null}

                    {/*------ all data show-------- */}
                    {this.state.Allshipment &&
                      data.map((item, index) => {
                        return (
                          <Card
                            key={index}
                            list={item}
                            detailPage="Shipments"
                          />
                        );
                      })}
                    {/*------- filter data show------- */}
                    {this.state.Filtershipment ? (
                      filterRecord.length > 0 ? (
                        filterRecord.map((item, index) => {
                          return (
                            <Card
                              key={index}
                              list={item}
                              detailPage="Shipments"
                            />
                          );
                        })
                      ) : (
                        <p className="mt-5">No Shipments found!</p>
                      )
                    ) : null}
                  </div>
                </div>
                <div className="col-md-3">
                  <div
                    className="sizeofmodule shadow-sm"
                    style={{ background: "#ffffff" }}
                  >
                    <span
                      style={{ alignSelf: "flex-start", marginBottom: 10 }}
                      className="light_gray_text"
                    >
                      FILTER SHIPMENTS
                    </span>
                    {/*---------------BUTTONS---------------------- */}
                    {buttontext.map((item) => {
                      return (
                        <FilterModule
                          key={item.id}
                          button_select={this.state.button_select}
                          onPressButton={this.onPressButton}
                          buttonText={item.text}
                        />
                      );
                    })}
                  </div>

                  {/* Add button for new shipment */}
                  <div className="positionrel">
                    <div className="plusbuttonpositon">
                      <Link to="/NewShipment">
                        <button className="btn btn--rounded px-4 shadow">
                          <span>+</span>
                        </button>
                      </Link>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </React.Fragment>
      );
    }
  }
}
const mapStateToProps = (state) => {
  return {
    customer_AllShipment: state.customerShipmentReducer.CustomerShipments,
    auth_Token: state.customerReducer.auth_Token,
    customer_id: state.customerReducer.Customer._id,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    getAllShipment: (
      requestBody,
      config,
      GetShipmentSucess,
      GetShipmentFailure
    ) =>
      dispatch(
        GetShipmentCustomerSide(
          requestBody,
          config,
          GetShipmentSucess,
          GetShipmentFailure
        )
      ),
    addCustomerDetails: (authToken, customer) =>
      dispatch(LoginAction({ authToken, customer })),
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(Shipments);

import React, { Component } from "react";
import { connect } from "react-redux";
import _ from "lodash";
import { Circle2 } from "react-preloaders";
import Images from "../../Pics/Images";
import NavBar from "../../Components/Common/NavBar";
import DetailInfoCard from "../../Components/CustomerSide/DetailInfoCard";
import CarrierPickupDel from "../../Components/CustomerSide/CarrierPickupDel";
import Progressbar from "../../Components/Common/Progressbar";
import OfferInformation from "../../Components/CustomerSide/OfferInformation";
import { LoginAction } from "../../Actions/actions";
import { getShipmentbyID } from "../../Actions/actioncreators";
import getCustomerOffer from "../../Services/getCustomerOffer";
import { getCustomer, getCustomerObj } from "../../LocalStorage/Auth";
import "../../Styles/DetailCusShip.css";

class ShippingDetailPage extends Component {
  state = {
    offerModal: false,
    shipmentid: this.props.match.params.id,
    shipmentobj: {},
    loading: true,
    offers: [],
  };

  componentDidMount() {
    let shipment;
    if (this.props.authToken === "") {
      //get user details from async storage
      const token = getCustomer();
      const customer = getCustomerObj();

      this.props.addCustomerDetails(token, customer);
    }

    if (
      (shipment = this.props.CustomerShipments.find(
        (el) => el._id === this.state.shipmentid
      ))
    ) {
      this.setState({ shipmentobj: shipment, loading: false });
      this.getOffersforshipment(shipment);
    } else {
      //call action creator here
      const config = {
        headers: {
          Authorization: `Bearer ${getCustomer()}`,
          "Content-Type": "application/json",
        },
      };
      this.props.getShipmentbyId(
        this.state.shipmentid,
        config,
        this.onSuccessCallback
      );
    }
  }

  getOffersforshipment(shipmentobj) {
    console.log(shipmentobj._id, "shipmentid");
    getCustomerOffer(
      shipmentobj._id,
      {
        headers: {
          Authorization: `Bearer ${getCustomer()}`,
          "Content-Type": "application/json",
        },
      },
      this.GetOfferSucess
    );
  }

  GetOfferSucess = (offers) => {
    console.log(offers, "Our Offers");
    this.setState({ offers: offers.data });
  };

  onSuccessCallback = (shipmentobj) => {
    console.log(shipmentobj, "shipmentobj");
    this.setState({ shipmentobj: shipmentobj, loading: false });
    this.getOffersforshipment(shipmentobj);
  };

  render() {
    const { shipmentobj } = this.state;
    console.log("check value type", shipmentobj.carrier);
    if (this.state.loading) {
      return (
        <Circle2
          color="black"
          background="#f4f6fc"
          time={0}
          customLoading={this.state.loading}
        />
      );
    }
    return (
      <React.Fragment>
        <NavBar />
        <div
          className="container-fluid"
          style={{
            overflowY: "auto",
            overflowX: "hidden",
            maxHeight: "90vh",
            msOverflowStyle: "none",
          }}
        >
          <div className="row">
            <div className="container pt-5">
              <h2 className="display-6 mb-5">Shipments Information</h2>
              <div className="row">
                {/*-------------Row Heading-------- */}
                <div className="col-6">
                  <div className="d-flex flex-row">
                    <div className="headingabovedetailcard">
                      {shipmentobj._id}
                    </div>
                    <div className="ml-auto">
                      <img
                        src={Images.pickupicon}
                        className="img-fluid d-inline"
                        alt="arrowup"
                        style={{ width: "30px", height: "30px" }}
                      ></img>
                      <span className="ml-2">
                        {shipmentobj.pickup_location
                          ? shipmentobj.pickup_location.address
                          : ""}
                      </span>
                    </div>
                  </div>
                </div>
                <div className="col-6">
                  <div className="d-flex justify-content-between">
                    <div>
                      <img
                        src={Images.deliveryicon}
                        className="img-fluid d-inline"
                        alt="arrowup"
                        style={{ width: "30px", height: "30px" }}
                      ></img>
                      <span className="ml-2">
                        {shipmentobj.dropoff_location
                          ? shipmentobj.dropoff_location.address
                          : ""}
                      </span>
                    </div>
                    <div>
                      
                      <div> {shipmentobj.shipment_status}</div>
                      <Progressbar barvalue={50} />
                    </div>
                  </div>
                </div>
              </div>

              <div className="row">
                <div className="col-lg-6 col-md-12 mt-2">
                  {/*---------1ST CARD-------------- */}
                  <DetailInfoCard shipment={shipmentobj} />
                </div>
                <div className="col-md-12 col-lg-6 mt-2">
                  {/*---------2ND CARD-------------- */}
                  {shipmentobj.shipment_status == "WAITING" ? (
                    <OfferInformation
                      offers={this.state.offers}
                      shipmentid={this.state.shipmentid}
                    />
                  ) : (
                    <CarrierPickupDel title="Carrier Informations" />
                  )}
                </div>
              </div>
              {/*-------if truck and driver assign other don't show----- */}
              {shipmentobj.selected_driver && shipmentobj.selected_truck ? (
                <div className="row my-5">
                  <div className="col-lg-6 col-md-12 mt-2">
                    {/*---------3RD CARD-------------- */}
                    <CarrierPickupDel title="Pickup Information" />
                  </div>
                  <div className="col-md-12 col-lg-6 mt-2">
                    {/*---------4TH CARD-------------- */}
                    <CarrierPickupDel title="Delivery Information" />
                  </div>
                </div>
              ) : (
                ""
              )}
            </div>
          </div>
        </div>
      </React.Fragment>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    CustomerShipments: state.customerShipmentReducer.CustomerShipments,
    offers: state.customerShipmentReducer.offers,
    authToken: state.customerReducer.auth_Token,
  };
};
const mapDispatchToProps = (dispatch) => {
  return {
    addCustomerDetails: (authToken, customer) =>
      dispatch(LoginAction({ authToken, customer })),
    getShipmentbyId: (shipmentid, config, onSuccessCallback) => {
      dispatch(getShipmentbyID(shipmentid, config, onSuccessCallback));
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(ShippingDetailPage);

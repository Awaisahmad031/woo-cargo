import React from "react";
import Joi from "joi-browser";
import InputField from "../Components/Common/InputField";
import logo from "../Pics/logo_with_text.svg";
import LoginBox from "../Components/Common/LoginBox";
import Form from "../Components/Common/Form";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { CustomerLogin, CarrierLogin } from "../Actions/actioncreators";
import {
  createLocalStorageCarrier,
  createLocalStorageCustomer,
  getCustomer,
  getCarrier,
} from "../LocalStorage/Auth";
import "../Styles/LogInForm.css";
class LogInForm extends Form {
  state = {
    data: { email: "", password: "" },
    error: {},
    link: "",
    box_clicked_customer: false,
    box_clicked_carrier: false,
    box_error: false,
    invalid_login: false,
    loading: false,
    loginButtonSpinner: false,
  };

  // schema for valiation
  schema = {
    email: Joi.string().email({ minDomainAtoms: 2 }).required().label("Email"),
    password: Joi.string().required().label("Password"),
  };
  //Boxes link
  box_callback_link = (link) => {
    if (link === "/Shipments")
      return this.setState({
        link: link,
        box_error: false,
        box_clicked_customer: true,
        box_clicked_carrier: false,
      });
    return this.setState({
      link: link,
      box_error: false,
      box_clicked_customer: false,
      box_clicked_carrier: true,
    });
  };
  spinnerButton = () => {
    this.setState({ loginButtonSpinner: true });
  };
  //Submit the form
  customerOrCarrierLogin = (e) => {
    e.preventDefault();
    this.spinnerButton();
    // either user select one of the box or not
    if (!this.state.link) return this.setState({ box_error: true });
    // validation of input fields
    const error = this.validate();
    this.setState({ error: error || {} });
    //if error occur don't call api
    if (error) return null;

    //call the api
    this.callServerApi();
  };
  callServerApi = () => {
    this.setState({ invalid_login: false });
    const requestBody = {
      email: this.state.data.email,
      password: this.state.data.password,
    };
    const config = {
      headers: {
        "Content-Type": "application/json",
      },
    };
    if (this.state.link === "/Shipments")
      this.props.login(
        requestBody,
        config,
        this.LoginSucessCustomer,
        this.LoginFailureCustomer
      );
    else
      this.props.loginCarrier(
        requestBody,
        config,
        this.LoginSucessCarrier,
        this.LoginFailureCarrier
      );
  };

  LoginSucessCustomer = (token, obj) => {
    createLocalStorageCustomer(token, obj);
    this.props.history.push("/Shipments");
  };
  LoginFailureCustomer = () => {
    this.setState({ invalid_login: true, loginButtonSpinner: false });
  };
  LoginSucessCarrier = (token, obj) => {
    createLocalStorageCarrier(token, obj);
    this.props.history.push("/CarrierHome");
  };
  LoginFailureCarrier = () => {
    this.setState({ invalid_login: true, loginButtonSpinner: false });
  };

  render() {
    if (getCustomer()) {
      this.props.history.push("/Shipments");
      return null;
    } else if (getCarrier()) {
      this.props.history.push("/CarrierHome");
      return null;
    } else
      return (
        <React.Fragment>
          <div
            className="container-fluid h-100"
            style={{ background: "#ffffff" }}
          >
            <div className="row h-100">
              <div className="logo--position d-inline">
                <img src={logo} className="img-fluid" alt="logo"></img>
              </div>
              <div className="col-md-4 offset-md-1">
                <div className="top__login--margin">
                  <div className="form__padding--smallscreen">
                    <span className="fontchange d-block">
                      Sign in to WooCargo
                    </span>
                    <small className="text--color">
                      Please enter your credentials to proceed.
                    </small>
                    {/*---------BOXES-----------  */}
                    <div className="boxes my-3 ">
                      <div className="d-flex">
                        <LoginBox
                          link={this.box_callback_link}
                          title="Customer"
                          box_clicked={this.state.box_clicked_customer}
                        />
                        <LoginBox
                          link={this.box_callback_link}
                          title="Carrier"
                          box_clicked={this.state.box_clicked_carrier}
                        />
                      </div>
                    </div>
                    {/*-------------BOXES Errors---------- */}
                    {this.state.box_error && (
                      <div
                        className="alert alert-danger"
                        style={{ fontSize: "14px" }}
                      >
                        Select Either Customer or Carrier from above
                      </div>
                    )}
                    {/*--------------Invalid Credentials--------  */}
                    {this.state.invalid_login && (
                      <div
                        className="alert alert-danger"
                        style={{ fontSize: "14px" }}
                      >
                        The email and password you entered did not match our
                        records. Please double-check and try again.
                      </div>
                    )}
                    {/*-----------------Form begins--------------*/}
                    <form onSubmit={this.customerOrCarrierLogin}>
                      <div className="form-group">
                        <InputField
                          labeltitle="EMAIL"
                          typename="email"
                          fieldname="email"
                          changeHandler={this.handleChange}
                          value={this.state.data.email}
                          error={this.state.error.email}
                        />
                      </div>
                      <div className="form-group">
                        <InputField
                          labeltitle="PASSWORD"
                          typename="password"
                          fieldname="password"
                          changeHandler={this.handleChange}
                          value={this.state.data.password}
                          error={this.state.error.password}
                        />
                      </div>
                      <button
                        disabled={
                          this.validate() || this.state.loginButtonSpinner
                        }
                        type="submit"
                        className="btn btn-primary btncolor btn-block btn-lg"
                      >
                        {this.state.loginButtonSpinner ? (
                          <span>Loading ...</span>
                        ) : (
                          <span>Sign in</span>
                        )}
                      </button>
                      <div className="text--color text-center pb-4">
                        <span>Dont't have an account</span> &nbsp;
                        <Link to="/SignUp" className="font-weight-bold">
                          Sign Up
                        </Link>
                      </div>
                    </form>
                    {/*-----------form End-----------  */}
                  </div>
                </div>
              </div>
              <div className="col-md-6 offset-md-1 pr-0">
                <div className="bgpic"></div>
              </div>
            </div>
          </div>
        </React.Fragment>
      );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    login: (requestBody, config, LoginSucessCustomer, LoginFailureCustomer) => {
      dispatch(
        CustomerLogin(
          requestBody,
          config,
          LoginSucessCustomer,
          LoginFailureCustomer
        )
      );
    },
    loginCarrier: (
      requestBody,
      config,
      LoginSucessCarrier,
      LoginFailureCarrier
    ) => {
      dispatch(
        CarrierLogin(
          requestBody,
          config,
          LoginSucessCarrier,
          LoginFailureCarrier
        )
      );
    },
  };
};
export default connect(null, mapDispatchToProps)(LogInForm);

import jwtDecode from "jwt-decode";
const Customer = "Customer_AuthToken";
const CustomerObj = "Customer_Obj";
const Carrier = "Carrier_AuthToken";
const CarrierObj = "Carrier_Obj";

export function createLocalStorageCustomer(token, obj) {
  localStorage.setItem(Customer, token);
  localStorage.setItem(CustomerObj, JSON.stringify(obj));
}
export function createLocalStorageCarrier(token, obj) {
  localStorage.setItem(Carrier, token);
  localStorage.setItem(CarrierObj, JSON.stringify(obj));
}
export function getCustomerObj() {
  try {
    const Object = JSON.parse(localStorage.getItem(CustomerObj));
    return Object;
  } catch (ex) {
    return null;
  }
}
export function getCarrierObj() {
  try {
    const Object = JSON.parse(localStorage.getItem(CarrierObj));
    return Object;
  } catch (ex) {
    return null;
  }
}
export function removeToken() {
  localStorage.clear();
  window.location = "/";
}
export function getCustomer() {
  try {
    const jwt = localStorage.getItem(Customer);
    return jwt;
  } catch (ex) {
    return null;
  }
}
export function getCarrier() {
  try {
    const jwt = localStorage.getItem(Carrier);
    return jwt;
  } catch (ex) {
    return null;
  }
}
export function getCustomerUserId() {
  try {
    const jwt = localStorage.getItem(Customer);
    return jwtDecode(jwt);
  } catch (ex) {
    return null;
  }
}
export function getCarrierUserId() {
  try {
    const jwt = localStorage.getItem(Carrier);
    return jwtDecode(jwt);
  } catch (ex) {
    return null;
  }
}
export function CustomerDetail(obj) {
  localStorage.setItem("Customer_Detail", JSON.stringify(obj));
}
export function getCustomerDetail() {
  try {
    const obj = JSON.parse(localStorage.getItem("Customer_Detail"));
    return obj;
  } catch (ex) {
    return null;
  }
}

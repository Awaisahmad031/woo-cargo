export const URL = "http://localhost:8000/";

const first_capital=word=>{
   return word.charAt(0).toUpperCase() + word.slice(1)
}

export const transform_shipment_attributes =(object)=>{
   let my_obj={}
   
   Object.keys(object).map(attribute=>{
       if(attribute!='_id'){
           let name=attribute;
           let value=object[attribute];
           if(attribute=='fromfloor')name='from Floor'
           if(attribute=='tofloor')name='To Floor'
           if(attribute=='trucktype')name='Truck Type'
           if(object[attribute]==true) value='Yes'
           if(object[attribute]==false) value='No'
           my_obj[first_capital(name)]=value;
       }
   })
   console.log(my_obj)
   return my_obj
   }